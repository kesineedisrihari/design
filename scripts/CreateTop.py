import re


def ExtractModulePorts(TopFileHandle):
	ModuleStart=0
	Ports=[]
	for Line in TopFileHandle:
		#Start Comment	
		if re.match(r'\s*\/\s*\/',Line):
			continue
	
		#End Comment
	
		#/**/ Expression
	
		if 'module' in Line:
			print Line
			ModuleStart=1
	
		if ModuleStart==1:
			m=re.match(r'\s*(input|output)\s*\[*\s*(\d*)\s*\:*\s*(\d*)\s*\]*\s*(\w+)',Line)
			if m is not None:
				Port={}
				Port['Dir']=m.group(1)
				if m.group(2) is None:			
					port['WIDTH']=1
				else:
					Port['WIDTH']=m.group(2)
				Port['Name']=m.group(4)	
				Port['socinout']=''
				Port['sigtype']=''	
				print Port
				Ports.append(Port)
		if ');' in Line:
			break
	return Ports

TopFile='../../soc/design/opencores/spi/src/spi.v'
TopFileHandle=open(TopFile,'r')

ExtractModulePorts(TopFileHandle)

