//  --========================================================================-- 
//  This confidential and proprietary software may be used only as 
//  authorised by a licensing agreement from ARM Limited 
//    (C) COPYRIGHT 2001-2002 ARM Limited 
//        ALL RIGHTS RESERVED 
//  The entire notice above must be reproduced on all authorised 
//  copies and copies may only be made to the extent permitted 
//  by a licensing agreement from ARM Limited. 
// 
//  ---------------------------------------------------------------------------- 
//  Version and Release Control Information: 
// 
//  File Name           : WdogPackage.v,v 
//  File Revision       : 1.4 
// 
//  Release Information : BP010-AC-20004-r1p0-03ltd0 
// 
//  ---------------------------------------------------------------------------- 
//  Purpose             : The constants used in the Watchdog module are defined 
//                        in this block. 
//  --========================================================================-- 
 
//------------------------------------------------------------------------------ 
// Watchdog register addresses 
//------------------------------------------------------------------------------ 
 
// Watchdog base address prefixes 
`define WDOG1A 7'b0000000 
`define WDOG2A 7'b0000001 
 
// Lock register base address 
`define WDOGLA 7'b1100000 
// Integration Test register base address 
`define WDOGIA 7'b1111000 
// Peripheral and PrimeCell register base address 
`define WDOGPA 7'b1111111 
 
// Register addresses for use by Frcs 
`define WDOGLOADA 3'b000 
`define WDOGVALUEA 3'b001 
`define WDOGCONTROLA 3'b010 
`define WDOGCLEARA 3'b011 
`define WDOGINTRAWA 3'b100 
`define WDOGINTA 3'b101 
 
// Watchdog Lock register 
`define WDOGLOCKA 3'b000 
 
// Integration Test registers 
`define WDOGTCRA 3'b000 
`define WDOGTOPA 3'b001 
 
// Peripheral and PrimeCell ID registers 
`define WPERIPHID0A 3'b000 
`define WPERIPHID1A 3'b001 
`define WPERIPHID2A 3'b010 
`define WPERIPHID3A 3'b011 
`define WPCELLID0A 3'b100 
`define WPCELLID1A 3'b101 
`define WPCELLID2A 3'b110 
`define WPCELLID3A 3'b111 
 
// --============================== End ======================================-- 
