module hdmi_tm(/*AUTOARG*/
  // Outputs
  hdmi_tm_data_10bit_out_r, hdmi_tm_read_enable,
  // Inputs
  hdmi_enable, clk_hdmi_slow, rst_hdmi_n, hdmi_tm_c0, hdmi_tm_c1,
  hdmi_tm_data, hdmi_tm_aux, hdmi_tm_vde, hdmi_tm_ade, hdmi_tm_enable
  );

  
  //TM-module inputs
  input                        hdmi_enable;
  input                        clk_hdmi_slow;                      //
  input                        rst_hdmi_n;                    //
  input                        hdmi_tm_c0;                       // CTL0-3 and H/Vsync
  input                        hdmi_tm_c1;                       // 
  input [7:0]                  hdmi_tm_data;                     // Video data
  input [3:0]                  hdmi_tm_aux;                      // Aux data
  input                        hdmi_tm_vde;                      // Video enabling bit
  input                        hdmi_tm_ade;                      // Aux enabling bit
  input                        hdmi_tm_enable;
  

  //TM-module outputs
  output [9:0] hdmi_tm_data_10bit_out_r; //
  output       hdmi_tm_read_enable;
 
  
  //Tm-module registers
 
  reg          hdmi_tm_enable_first_r;
  reg          hdmi_tm_enable_second_r;
  reg [3:0]          hdmi_tm_nrof1_counter1;   //counters for determining TM and DS modes
  reg [3:0]          hdmi_tm_nrof1_buffer1_r;
  reg [3:0]          hdmi_tm_nrof1_counter2;
  reg [3:0]          hdmi_tm_nrof1_buffer2_r;
  reg [3:0]          hdmi_tm_nrof0_buffer2_r;
  reg [7:0]          hdmi_tm_data_8bit_buffer_r; // Buffers for transposing data
  reg [8:0]          hdmi_tm_data_8to9_buffer; 
  reg [8:0]          hdmi_tm_data_9bit_buffer_r;
  reg [9:0]          hdmi_tm_data_9to10_buffer;
  reg [9:0]          hdmi_tm_data_10bit_out_r;
  reg [3:0]          hdmi_tm_aux_first_buffer_r;
  reg [3:0]          hdmi_tm_aux_second_buffer_r;
  reg [1:0]          hdmi_tm_cc_first_buffer_r;
  reg [1:0]          hdmi_tm_cc_second_buffer_r;
  reg [1:0]          hdmi_tm_first_mode_r;
  reg [1:0]          hdmi_tm_second_mode_r;
  reg [4:0]          hdmi_tm_total_counter_r;
  reg [4:0]          hdmi_tm_temp_counter;
  reg                hdmi_tm_read_enable;

 /*AUTOINPUT*/
 /*AUTOOUTPUT*/
 /*AUTOINOUT*/
 /*AUTOWIRE*/
 /*AUTOREG*/
 
// Beginning of automatic regs (for this module's undeclared outputs)

 // End of automatics

  
/*******************************************************************************************/
  always @ (posedge clk_hdmi_slow or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_tm_aux_first_buffer_r <= 4'h0;
      hdmi_tm_aux_second_buffer_r <= 4'h0;
      hdmi_tm_cc_first_buffer_r <= 2'h0;
      hdmi_tm_cc_second_buffer_r <= 2'h0;
      hdmi_tm_data_10bit_out_r <= 10'h0;
      hdmi_tm_data_8bit_buffer_r <= 8'h0;
      hdmi_tm_data_9bit_buffer_r <= 9'h0;
      hdmi_tm_enable_first_r <= 1'h0;
      hdmi_tm_enable_second_r <= 1'h0;
      hdmi_tm_first_mode_r <= 2'h0;
      hdmi_tm_nrof0_buffer2_r <= 4'h0;
      hdmi_tm_nrof1_buffer1_r <= 4'h0;
      hdmi_tm_nrof1_buffer2_r <= 4'h0;
      hdmi_tm_read_enable <= 1'h0;
      hdmi_tm_second_mode_r <= 2'h0;
      hdmi_tm_total_counter_r <= 5'h0;
      // End of automatics

    end

    else begin
      if (hdmi_tm_enable == 1'b1) begin
        
        if((hdmi_enable == 1'b0) & (hdmi_tm_enable_first_r == 1'b0) & 
          (hdmi_tm_enable_second_r == 1'b0)) begin
          hdmi_tm_aux_first_buffer_r   <= {(1+(3)){1'b0}};
          hdmi_tm_aux_second_buffer_r  <= {(1+(3)){1'b0}};
          hdmi_tm_cc_first_buffer_r    <= 2'b0;
          hdmi_tm_cc_second_buffer_r   <= 2'b0;         
          hdmi_tm_data_10bit_out_r     <= 10'h0;
          hdmi_tm_data_8bit_buffer_r   <= 8'h0;
          hdmi_tm_data_9bit_buffer_r   <= 9'h0;
          hdmi_tm_first_mode_r         <= 2'h0;
          hdmi_tm_nrof0_buffer2_r      <= 4'h0;
          hdmi_tm_nrof1_buffer1_r      <= 4'h0;
          hdmi_tm_nrof1_buffer2_r      <= 4'h0;
          hdmi_tm_second_mode_r        <= 2'h0;
        end //
        else begin
          hdmi_tm_enable_second_r      <= hdmi_tm_enable_first_r;
          hdmi_tm_enable_first_r       <= hdmi_enable;
          hdmi_tm_data_8bit_buffer_r   <= hdmi_tm_data;
          hdmi_tm_data_9bit_buffer_r   <= hdmi_tm_data_8to9_buffer;
          hdmi_tm_data_10bit_out_r     <= hdmi_tm_data_9to10_buffer; // Setting prev calc 
          hdmi_tm_aux_second_buffer_r  <= hdmi_tm_aux_first_buffer_r; // Shifting aux buffer
          hdmi_tm_aux_first_buffer_r   <= hdmi_tm_aux;
          hdmi_tm_cc_second_buffer_r   <= hdmi_tm_cc_first_buffer_r;
          hdmi_tm_cc_first_buffer_r[1] <= hdmi_tm_c1;
          hdmi_tm_cc_first_buffer_r[0] <= hdmi_tm_c0;          
          hdmi_tm_nrof1_buffer1_r      <= hdmi_tm_nrof1_counter1;
          hdmi_tm_nrof1_buffer2_r      <= hdmi_tm_nrof1_counter2;
          hdmi_tm_nrof0_buffer2_r      <= (4'h8-hdmi_tm_nrof1_counter2);
          hdmi_tm_second_mode_r[1]     <= hdmi_tm_first_mode_r[1]; // Copy last op 
          hdmi_tm_second_mode_r[0]     <= hdmi_tm_first_mode_r[0]; //
          hdmi_tm_first_mode_r[1]      <= hdmi_tm_vde; // 
          hdmi_tm_first_mode_r[0]      <= hdmi_tm_ade;
        end // else: !if(hdmi_enable == 1'b0 and hdmi_enable_first == 1'b0 and...
        
        
        if(hdmi_tm_enable_second_r == 1'b1) begin
          hdmi_tm_total_counter_r    <= hdmi_tm_temp_counter;
          hdmi_tm_read_enable        <= 1'b1; // Enables FIFO
        end
        else begin
          hdmi_tm_read_enable        <= 1'b0; // Disables FIFO
          hdmi_tm_total_counter_r    <= 5'h0; // If starting up..
        end
//        hdmi_tm_counter_out_r        <= hdmi_tm_total_counter_r;   
      end     // 
      else begin
//TESTING*************************
        hdmi_tm_read_enable <= 1'b0;  // Will stop fifo from reading old values
//TESTING*************************
      end // else: !if(hdmi_tm_enable == 1'b1)
      
    end // else: !if(rst_hdmi_tm_n == 1'b0)
  end // always @ (posedge clk_hdmi_tm_gated or negedge rst_hdmi_tm_n)


/*******************************************************************************************/
                                      //TM CALCULATIONS
/*******************************************************************************************/    
  always @ (*) begin

    if (hdmi_tm_enable_first_r == 1'b1 && hdmi_tm_enable == 1'b1) begin
    
      hdmi_tm_nrof1_counter1 = hdmi_tm_data[7] + hdmi_tm_data[6] + hdmi_tm_data[5] + 
                              hdmi_tm_data[4] + hdmi_tm_data[3] + hdmi_tm_data[2] + 
                              hdmi_tm_data[1] + hdmi_tm_data[0]; // Always, just in case
      
      case(hdmi_tm_first_mode_r) 
        //VIDEO ENCODING
        2'b10: begin                      
          //Choosing XOR or XNOR        
          
          
          //XNOR
          if(hdmi_tm_nrof1_buffer1_r > 4'h4 | (hdmi_tm_nrof1_buffer1_r == 4'h4 & 
                                             hdmi_tm_data_8bit_buffer_r[0] == 1'b0)) begin
            hdmi_tm_data_8to9_buffer[0] = hdmi_tm_data_8bit_buffer_r[0];
            hdmi_tm_data_8to9_buffer[1] = (hdmi_tm_data_8to9_buffer[0] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[1]);
            hdmi_tm_data_8to9_buffer[2] = (hdmi_tm_data_8to9_buffer[1] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[2]);
            hdmi_tm_data_8to9_buffer[3] = (hdmi_tm_data_8to9_buffer[2] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[3]);
            hdmi_tm_data_8to9_buffer[4] = (hdmi_tm_data_8to9_buffer[3] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[4]);
            hdmi_tm_data_8to9_buffer[5] = (hdmi_tm_data_8to9_buffer[4] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[5]);
            hdmi_tm_data_8to9_buffer[6] = (hdmi_tm_data_8to9_buffer[5] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[6]);
            hdmi_tm_data_8to9_buffer[7] = (hdmi_tm_data_8to9_buffer[6] ~^ 
                                            hdmi_tm_data_8bit_buffer_r[7]);
            hdmi_tm_data_8to9_buffer[8] = 1'b0; // TM-flag = 0 means XNOR'ed
          end // 

          //XOR
          else  begin
            hdmi_tm_data_8to9_buffer[0] = hdmi_tm_data_8bit_buffer_r[0];
            hdmi_tm_data_8to9_buffer[1] = (hdmi_tm_data_8to9_buffer[0] ^ 
                                            hdmi_tm_data_8bit_buffer_r[1]);
            hdmi_tm_data_8to9_buffer[2] = (hdmi_tm_data_8to9_buffer[1] ^ 
                                            hdmi_tm_data_8bit_buffer_r[2]);
            hdmi_tm_data_8to9_buffer[3] = (hdmi_tm_data_8to9_buffer[2] ^ 
                                            hdmi_tm_data_8bit_buffer_r[3]);
            hdmi_tm_data_8to9_buffer[4] = (hdmi_tm_data_8to9_buffer[3] ^ 
                                            hdmi_tm_data_8bit_buffer_r[4]);
            hdmi_tm_data_8to9_buffer[5] = (hdmi_tm_data_8to9_buffer[4] ^ 
                                            hdmi_tm_data_8bit_buffer_r[5]);
            hdmi_tm_data_8to9_buffer[6] = (hdmi_tm_data_8to9_buffer[5] ^ 
                                            hdmi_tm_data_8bit_buffer_r[6]);
            hdmi_tm_data_8to9_buffer[7] = (hdmi_tm_data_8to9_buffer[6] ^ 
                                            hdmi_tm_data_8bit_buffer_r[7]);
            hdmi_tm_data_8to9_buffer[8] = 1'b1; // TM-flag = 1 means XOR'ed
          end // if (hdmi_tm_xnor_select == 1'b0)

          hdmi_tm_nrof1_counter2 = hdmi_tm_data_8to9_buffer[7] + hdmi_tm_data_8to9_buffer[6] + 
                                    hdmi_tm_data_8to9_buffer[5] + hdmi_tm_data_8to9_buffer[4] + 
                                    hdmi_tm_data_8to9_buffer[3] + hdmi_tm_data_8to9_buffer[2] + 
                                    hdmi_tm_data_8to9_buffer[1] + hdmi_tm_data_8to9_buffer[0]; 
          
        end // case: 10
        

        default: begin                     // Control period
          hdmi_tm_data_8to9_buffer = 9'h0;
          hdmi_tm_nrof1_counter2 = 4'h0; 
        end //default
      endcase // case (hdmi_tm_mode)
    end // if (hdmi_tm_enable_first == 1'b1)
    else begin
      hdmi_tm_data_8to9_buffer = 9'h0;
      hdmi_tm_nrof1_counter1 = 4'h0; 
      hdmi_tm_nrof1_counter2 = 4'h0; 
    end                              // 
      
    
  end // always @ (*)

  
/*******************************************************************************************/
                                      // DS CALCULATIONS
/*******************************************************************************************/
  always @ (*) begin
    if (hdmi_tm_enable_second_r == 1'b1 && hdmi_tm_enable == 1'b1) begin
      case (hdmi_tm_second_mode_r)

        2'b10: begin //DC-balance only for Video. Aux and Ctrl are premade balanced
          
          if(hdmi_tm_total_counter_r == 5'h0 | 
             hdmi_tm_nrof1_buffer2_r == hdmi_tm_nrof0_buffer2_r) begin
            hdmi_tm_data_9to10_buffer[9] = ~hdmi_tm_data_9bit_buffer_r[8];
            hdmi_tm_data_9to10_buffer[8] = hdmi_tm_data_9bit_buffer_r[8];
            hdmi_tm_data_9to10_buffer[7:0] = (hdmi_tm_data_9bit_buffer_r[8] ? 
                                               hdmi_tm_data_9bit_buffer_r[7:0]:
                                               ~hdmi_tm_data_9bit_buffer_r[7:0]);
            if(hdmi_tm_data_9bit_buffer_r[8] == 1'b0) begin
              hdmi_tm_temp_counter = hdmi_tm_total_counter_r + (hdmi_tm_nrof0_buffer2_r - 
                                                               hdmi_tm_nrof1_buffer2_r);
            end
            else begin
              hdmi_tm_temp_counter = hdmi_tm_total_counter_r + (hdmi_tm_nrof1_buffer2_r - 
                                                               hdmi_tm_nrof0_buffer2_r);
            end
            
          end
          else begin
            if (((hdmi_tm_total_counter_r[4] == 1'b0 & hdmi_tm_total_counter_r != 0) & 
                 hdmi_tm_nrof1_buffer2_r > hdmi_tm_nrof0_buffer2_r) |
                (hdmi_tm_total_counter_r[4] == 1'b1 & hdmi_tm_nrof1_buffer2_r < 
                 hdmi_tm_nrof0_buffer2_r)) 
              begin
                hdmi_tm_data_9to10_buffer[9] = 1'b1;
                hdmi_tm_data_9to10_buffer[8] = hdmi_tm_data_9bit_buffer_r[8];
                hdmi_tm_data_9to10_buffer[7:0] = ~hdmi_tm_data_9bit_buffer_r[7:0];

                hdmi_tm_temp_counter = hdmi_tm_total_counter_r + 
                                        {3'b000,hdmi_tm_data_9bit_buffer_r[8],1'b0} +
                                        (hdmi_tm_nrof0_buffer2_r - hdmi_tm_nrof1_buffer2_r);

              end // if (((hdmi_tm_total_counter[4] == 1'b0 & hdmi_tm_total_counter != 0) &...
            
            
            else begin                  // 
              hdmi_tm_data_9to10_buffer[9] = 1'b0;
              hdmi_tm_data_9to10_buffer[8] = hdmi_tm_data_9bit_buffer_r[8];
              hdmi_tm_data_9to10_buffer[7:0] = hdmi_tm_data_9bit_buffer_r[7:0];

              hdmi_tm_temp_counter = hdmi_tm_total_counter_r + 
                                      {{4{hdmi_tm_data_9bit_buffer_r[8]}},1'b0} +
                                      hdmi_tm_nrof1_buffer2_r  - hdmi_tm_nrof0_buffer2_r;

            end // else: !if
          end
          //                                                            
        end // else: !if(hdmi_tm_total_counter == 0 |...
        
        
        2'b01: begin
          case(hdmi_tm_aux_second_buffer_r)
            4'b0000: hdmi_tm_data_9to10_buffer = 10'b1010011100;
            4'b0001: hdmi_tm_data_9to10_buffer = 10'b1001100011;
            4'b0010: hdmi_tm_data_9to10_buffer = 10'b1011100100;
            4'b0011: hdmi_tm_data_9to10_buffer = 10'b1011100010;
            4'b0100: hdmi_tm_data_9to10_buffer = 10'b0101110001;
            4'b0101: hdmi_tm_data_9to10_buffer = 10'b0100011110;
            4'b0110: hdmi_tm_data_9to10_buffer = 10'b0110001110;
            4'b0111: hdmi_tm_data_9to10_buffer = 10'b0100111100;
            4'b1000: hdmi_tm_data_9to10_buffer = 10'b1011001100;
            4'b1001: hdmi_tm_data_9to10_buffer = 10'b0100111001;
            4'b1010: hdmi_tm_data_9to10_buffer = 10'b0110011100;
            4'b1011: hdmi_tm_data_9to10_buffer = 10'b1011000110;
            4'b1100: hdmi_tm_data_9to10_buffer = 10'b1010001110;
            4'b1101: hdmi_tm_data_9to10_buffer = 10'b1001110001;
            4'b1110: hdmi_tm_data_9to10_buffer = 10'b0101100011;
            4'b1111: hdmi_tm_data_9to10_buffer = 10'b1011000011;
            default: hdmi_tm_data_9to10_buffer = 10'b0000011111;
          endcase // case (hdmi_tm_aux_buffer)
          hdmi_tm_temp_counter = 4'h0;
          
        end                             // case: 01

        default: begin                     // Control period
          case(hdmi_tm_aux_second_buffer_r)
            4'b0000: hdmi_tm_data_9to10_buffer = 10'b1101010100;
            4'b0001: hdmi_tm_data_9to10_buffer = 10'b0010101011;
            4'b0010: hdmi_tm_data_9to10_buffer = 10'b0101010100;
            4'b0011: hdmi_tm_data_9to10_buffer = 10'b1010101011;
            default: hdmi_tm_data_9to10_buffer = 10'b0000011111; // Default at start 
          endcase
          hdmi_tm_temp_counter = 5'h0;
        end
      endcase // case (hdmi_tm_second_mode)
    end // if (hdmi_tm_enable_second == 1'b1)
    else begin
      hdmi_tm_data_9to10_buffer = 10'b0000011111;
      hdmi_tm_temp_counter = 5'h0;
    end // else: !if(hdmi_tm_enable_second_r == 1'b1)
    
    
  end
  
endmodule // hdmi_tm
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// )
// End:
