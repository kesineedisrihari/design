module hdmi_serializer(/*AUTOARG*/
  // Outputs
  hdmi_ser_ch0_out_p, hdmi_ser_ch1_out_p, hdmi_ser_ch2_out_p,
  hdmi_ser_ch0_out_n, hdmi_ser_ch1_out_n, hdmi_ser_ch2_out_n,
  hdmi_ser_runflag, hdmi_ser_count_out, hdmi_ser_ack, hdmi_ser_tmdsclk_p,
  hdmi_ser_tmdsclk_n,
  // Inputs
  clk_hdmi_fast, rst_hdmi_n, hdmi_fifo_90bit_out, hdmi_synchronized_ready
  );

  /*AUTOINPUT*/
  /*AUTOOUTPUT*/

  //HDMI SERIALIZER INPUT
  input                clk_hdmi_fast;
  input                rst_hdmi_n;
  input [89:0]         hdmi_fifo_90bit_out;
  input                hdmi_synchronized_ready;

  //HMDI SERIALIZER OUTPUT
  output               hdmi_ser_ch0_out_p;
  output               hdmi_ser_ch1_out_p;
  output               hdmi_ser_ch2_out_p;
  output               hdmi_ser_ch0_out_n;
  output               hdmi_ser_ch1_out_n;
  output               hdmi_ser_ch2_out_n;
  output               hdmi_ser_runflag; // Testing
  output [7:0]         hdmi_ser_count_out; // testing
  output               hdmi_ser_ack;
  output               hdmi_ser_tmdsclk_p;
  output               hdmi_ser_tmdsclk_n;

  /*AUTOREG*/
  /*AUTOWIRE*/
  
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg                   hdmi_ser_ch0_out_n;
  reg                   hdmi_ser_ch0_out_p;
  reg                   hdmi_ser_ch1_out_n;
  reg                   hdmi_ser_ch1_out_p;
  reg                   hdmi_ser_ch2_out_n;
  reg                   hdmi_ser_ch2_out_p;
  reg                   hdmi_ser_ack;
  reg                   hdmi_ser_tmdsclk_p;
  reg                   hdmi_ser_tmdsclk_n;

  // End of automatics

  wire [89:0]           hdmi_fifo_90bit_out;
  
  //HDMI SERIALIZER REGISTERS
  reg [92:0]           hdmi_ser_93bit_out_r;
  reg [7:0]            hdmi_ser_pos_counter;
  reg                  hdmi_ser_runflag;
  reg [89:0]           hdmi_fifo_90bit_out_r;
  reg [7:0]            hdmi_ser_count_out; // testing
  
  always @ (posedge clk_hdmi_fast or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_fifo_90bit_out_r <= 90'h0;
      hdmi_ser_93bit_out_r <= 93'h0;
      hdmi_ser_ack <= 1'h0;
      hdmi_ser_ch0_out_n <= 1'h0;
      hdmi_ser_ch0_out_p <= 1'h0;
      hdmi_ser_ch1_out_n <= 1'h0;
      hdmi_ser_ch1_out_p <= 1'h0;
      hdmi_ser_ch2_out_n <= 1'h0;
      hdmi_ser_ch2_out_p <= 1'h0;
      hdmi_ser_count_out <= 8'h0;
      hdmi_ser_pos_counter <= 8'h0;
      hdmi_ser_runflag <= 1'h0;
      hdmi_ser_tmdsclk_n <= 1'h0;
      hdmi_ser_tmdsclk_p <= 1'h1;
      // End of automatics
    end
    else begin
      hdmi_ser_count_out <= hdmi_ser_pos_counter;
      if(hdmi_ser_runflag == 1'b1 && (hdmi_ser_pos_counter == 8'd0 || hdmi_ser_pos_counter == 8'd5 
                                      || hdmi_ser_pos_counter == 8'd10 || hdmi_ser_pos_counter == 8'd15 
                                      || hdmi_ser_pos_counter == 8'd20 || hdmi_ser_pos_counter == 8'd25)) begin
        hdmi_ser_tmdsclk_p <= ~hdmi_ser_tmdsclk_p;
        hdmi_ser_tmdsclk_n <= ~hdmi_ser_tmdsclk_n;
       
      end
      

/*******************************************************************************************/
      //RUNNING
/*******************************************************************************************/
   

      if (hdmi_ser_runflag == 1'b1) begin
        hdmi_fifo_90bit_out_r <= hdmi_fifo_90bit_out;
        
        if (hdmi_ser_pos_counter > 8'd28) begin
          if (hdmi_synchronized_ready == 1'b0) begin // Changed from rdy_r
            hdmi_ser_runflag <= 1'b0;
          end
          else begin
            hdmi_ser_runflag <= 1'b1;
            hdmi_ser_pos_counter <= 8'h0;

            hdmi_ser_93bit_out_r[0] <= hdmi_ser_93bit_out_r[1];
            hdmi_ser_93bit_out_r[31] <= hdmi_ser_93bit_out_r[32];
            hdmi_ser_93bit_out_r[62] <= hdmi_ser_93bit_out_r[63];
            
            hdmi_ser_93bit_out_r[30:1]  <= hdmi_fifo_90bit_out_r[29:0];
            hdmi_ser_93bit_out_r[61:32] <= hdmi_fifo_90bit_out_r[59:30];
            hdmi_ser_93bit_out_r[92:63] <= hdmi_fifo_90bit_out_r[89:60];
            hdmi_ser_ack <= 1'b1; // Sending acknowledge to FIFO
          end
        end // if (hdmi_ser_pos_counter > 8'd28)
        
        else begin
          if(hdmi_ser_pos_counter == 8'd14) begin
            hdmi_ser_ack <= 1'b0;
          end
          
          hdmi_ser_runflag <= 1'b1;
          hdmi_ser_93bit_out_r <= hdmi_ser_93bit_out_r >> 1; 
          hdmi_ser_pos_counter <= hdmi_ser_pos_counter + 1;
        
        end // else: !if(hdmi_ser_pos_counter == 8'd30)

        hdmi_ser_ch0_out_n <= ~hdmi_ser_93bit_out_r[0];
        hdmi_ser_ch0_out_p <= hdmi_ser_93bit_out_r[0];
        hdmi_ser_ch1_out_n <= ~hdmi_ser_93bit_out_r[31];
        hdmi_ser_ch1_out_p <= hdmi_ser_93bit_out_r[31];
        hdmi_ser_ch2_out_n <= ~hdmi_ser_93bit_out_r[62];
        hdmi_ser_ch2_out_p <= hdmi_ser_93bit_out_r[62];
      end // if (hdmi_ser_runflag == 1'b1)
      
/*******************************************************************************************/
      //IDLE      
/*******************************************************************************************/

      else begin
        if (hdmi_synchronized_ready == 1'b1) begin // changed from rdy_r
          hdmi_fifo_90bit_out_r <= hdmi_fifo_90bit_out;
          hdmi_ser_runflag <= 1'b1;
          hdmi_ser_pos_counter <= 8'd29;
          hdmi_ser_ack <= 1'b1;
        end
        else begin
          hdmi_ser_runflag <= 1'b0;
          hdmi_ser_ack <=1'b1;
        end
      end
    end // else: !if(rst_hdmi_ser_n == 1'b0)
  end // always @ (posedge clk_hdmi_ser or negedge rst_hdmi_ser_n)
  
endmodule
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// )
// End:
