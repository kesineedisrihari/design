module hdmi_top_module(/*AUTOARG*/
  // Outputs
  hdmi_ser_tmdsclk_p, hdmi_ser_tmdsclk_n, hdmi_ser_runflag,
  hdmi_ser_count_out, hdmi_tm_data_10bit_out_r0, hdmi_tm_data_10bit_out_r1,
  hdmi_tm_data_10bit_out_r2, hdmi_fifo_ready, hdmi_synchronized_ack,
  hdmi_ser_ch0_out_p, hdmi_ser_ch0_out_n, hdmi_ser_ch1_out_p,
  hdmi_ser_ch1_out_n, hdmi_ser_ch2_out_p, hdmi_ser_ch2_out_n,
  hdmi_fifo_counter_out,
  // Inputs
  hdmi_tm_aux, hdmi_tm_data0, hdmi_tm_data1, hdmi_tm_data2, rst_hdmi_n,
  clk_hdmi_slow, clk_hdmi_fast, hdmi_enable, hdmi_tm_c0, hdmi_tm_c1,
  hdmi_tm_vde, hdmi_tm_ade
  );
  
  input [7:0]         hdmi_tm_data0;
  input [7:0]         hdmi_tm_data1;
  input [7:0]         hdmi_tm_data2;
  input               rst_hdmi_n;
  input               clk_hdmi_slow;
  input               clk_hdmi_fast;
  input               hdmi_enable;
  input               hdmi_tm_c0;
  input               hdmi_tm_c1;
  input               hdmi_tm_vde;
  input               hdmi_tm_ade;
  //input               hdmi_tm_enable;
  
  output [9:0]               hdmi_tm_data_10bit_out_r0;
  output [9:0]               hdmi_tm_data_10bit_out_r1;
  output [9:0]               hdmi_tm_data_10bit_out_r2; // 

  output                     hdmi_fifo_ready;
  output                     hdmi_synchronized_ack;
  
  
  output              hdmi_ser_ch0_out_p;
  output              hdmi_ser_ch0_out_n;
  output              hdmi_ser_ch1_out_p;
  output              hdmi_ser_ch1_out_n;
  output              hdmi_ser_ch2_out_p;
  output              hdmi_ser_ch2_out_n;
  output [2:0]        hdmi_fifo_counter_out;
 

  /*AUTOINPUT*/
  // Beginning of automatic inputs (from unused autoinst inputs)
  input [3:0]         hdmi_tm_aux;            // To U_HDMI_TM0 of hdmi_tm.v, ...
  // End of automatics
  /*AUTOOUTPUT*/
  // Beginning of automatic outputs (from unused autoinst outputs)
  output [7:0]        hdmi_ser_count_out;     // From U_HDMI_SER of hdmi_serializer.v
  output              hdmi_ser_runflag;       // From U_HDMI_SER of hdmi_serializer.v
  output              hdmi_ser_tmdsclk_n;     // From U_HDMI_SER of hdmi_serializer.v
  output              hdmi_ser_tmdsclk_p;     // From U_HDMI_SER of hdmi_serializer.v
  // End of automatics
  /*AUTOINOUT*/
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire [89:0]         hdmi_fifo_90bit_out;    // From U_HDMI_FIFO of hdmi_fifo.v
  wire                hdmi_ser_ack;           // From U_HDMI_SER of hdmi_serializer.v
  wire                hdmi_synchronized_ready;// From U_HDMI_SYNC of hdmi_sync.v
  wire                hdmi_tm_enable;         // From U_HDMI_FIFO of hdmi_fifo.v
  wire                hdmi_tm_read_enable0;   // From U_HDMI_TM0 of hdmi_tm.v
  wire                hdmi_tm_read_enable1;   // From U_HDMI_TM1 of hdmi_tm.v
  wire                hdmi_tm_read_enable2;   // From U_HDMI_TM2 of hdmi_tm.v
  // End of automatics
  /*AUTOREG*/
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg [2:0]           hdmi_fifo_counter_out;
  // End of automatics

  /* hdmi_tm AUTO_TEMPLATE(
   .hdmi_tm_data_10bit\(.*\) (hdmi_tm_data_10bit\1@[9:0]), 
   .hdmi_tm_read_enable\(.*\) (hdmi_tm_read_enable\1@),
   .hdmi_tm_data\(.*\) (hdmi_tm_data\1@[]), 
  
   ); */
  hdmi_tm U_HDMI_TM0(.clk_hdmi_slow    (clk_hdmi_slow), 
                      /*AUTOINST*/
                     // Outputs
                     .hdmi_tm_data_10bit_out_r(hdmi_tm_data_10bit_out_r0[9:0]), // Templated
                     .hdmi_tm_read_enable(hdmi_tm_read_enable0), // Templated
                     // Inputs
                     .hdmi_enable       (hdmi_enable),
                     .rst_hdmi_n        (rst_hdmi_n),
                     .hdmi_tm_c0        (hdmi_tm_c0),
                     .hdmi_tm_c1        (hdmi_tm_c1),
                     .hdmi_tm_data      (hdmi_tm_data0[7:0]),    // Templated
                     .hdmi_tm_aux       (hdmi_tm_aux[3:0]),
                     .hdmi_tm_vde       (hdmi_tm_vde),
                     .hdmi_tm_ade       (hdmi_tm_ade),
                     .hdmi_tm_enable    (hdmi_tm_enable));

  hdmi_tm U_HDMI_TM1(/*AUTOINST*/
                     // Outputs
                     .hdmi_tm_data_10bit_out_r(hdmi_tm_data_10bit_out_r1[9:0]), // Templated
                     .hdmi_tm_read_enable(hdmi_tm_read_enable1), // Templated
                     // Inputs
                     .hdmi_enable       (hdmi_enable),
                     .clk_hdmi_slow     (clk_hdmi_slow),
                     .rst_hdmi_n        (rst_hdmi_n),
                     .hdmi_tm_c0        (hdmi_tm_c0),
                     .hdmi_tm_c1        (hdmi_tm_c1),
                     .hdmi_tm_data      (hdmi_tm_data1[7:0]),    // Templated
                     .hdmi_tm_aux       (hdmi_tm_aux[3:0]),
                     .hdmi_tm_vde       (hdmi_tm_vde),
                     .hdmi_tm_ade       (hdmi_tm_ade),
                     .hdmi_tm_enable    (hdmi_tm_enable));

  hdmi_tm U_HDMI_TM2(/*AUTOINST*/
                     // Outputs
                     .hdmi_tm_data_10bit_out_r(hdmi_tm_data_10bit_out_r2[9:0]), // Templated
                     .hdmi_tm_read_enable(hdmi_tm_read_enable2), // Templated
                     // Inputs
                     .hdmi_enable       (hdmi_enable),
                     .clk_hdmi_slow     (clk_hdmi_slow),
                     .rst_hdmi_n        (rst_hdmi_n),
                     .hdmi_tm_c0        (hdmi_tm_c0),
                     .hdmi_tm_c1        (hdmi_tm_c1),
                     .hdmi_tm_data      (hdmi_tm_data2[7:0]),    // Templated
                     .hdmi_tm_aux       (hdmi_tm_aux[3:0]),
                     .hdmi_tm_vde       (hdmi_tm_vde),
                     .hdmi_tm_ade       (hdmi_tm_ade),
                     .hdmi_tm_enable    (hdmi_tm_enable));

  hdmi_fifo U_HDMI_FIFO(/*AUTOINST*/
                        // Outputs
                        .hdmi_fifo_90bit_out(hdmi_fifo_90bit_out[89:0]),
                        .hdmi_fifo_ready(hdmi_fifo_ready),
                        .hdmi_tm_enable (hdmi_tm_enable),
                        // Inputs
                        .clk_hdmi_slow  (clk_hdmi_slow),
                        .rst_hdmi_n     (rst_hdmi_n),
                        .hdmi_tm_data_10bit_out_r0(hdmi_tm_data_10bit_out_r0[9:0]),
                        .hdmi_tm_data_10bit_out_r1(hdmi_tm_data_10bit_out_r1[9:0]),
                        .hdmi_tm_data_10bit_out_r2(hdmi_tm_data_10bit_out_r2[9:0]),
                        .hdmi_synchronized_ack(hdmi_synchronized_ack),
                        .hdmi_tm_read_enable0(hdmi_tm_read_enable0),
                        .hdmi_tm_read_enable1(hdmi_tm_read_enable1),
                        .hdmi_tm_read_enable2(hdmi_tm_read_enable2));
 
  hdmi_sync U_HDMI_SYNC(/*AUTOINST*/
                        // Outputs
                        .hdmi_synchronized_ready(hdmi_synchronized_ready),
                        .hdmi_synchronized_ack(hdmi_synchronized_ack),
                        // Inputs
                        .clk_hdmi_slow  (clk_hdmi_slow),
                        .clk_hdmi_fast  (clk_hdmi_fast),
                        .rst_hdmi_n     (rst_hdmi_n),
                        .hdmi_fifo_ready(hdmi_fifo_ready),
                        .hdmi_ser_ack   (hdmi_ser_ack));

  hdmi_serializer U_HDMI_SER(/*AUTOINST*/
                             // Outputs
                             .hdmi_ser_ch0_out_p(hdmi_ser_ch0_out_p),
                             .hdmi_ser_ch1_out_p(hdmi_ser_ch1_out_p),
                             .hdmi_ser_ch2_out_p(hdmi_ser_ch2_out_p),
                             .hdmi_ser_ch0_out_n(hdmi_ser_ch0_out_n),
                             .hdmi_ser_ch1_out_n(hdmi_ser_ch1_out_n),
                             .hdmi_ser_ch2_out_n(hdmi_ser_ch2_out_n),
                             .hdmi_ser_runflag  (hdmi_ser_runflag),
                             .hdmi_ser_count_out(hdmi_ser_count_out[7:0]),
                             .hdmi_ser_ack      (hdmi_ser_ack),
                             .hdmi_ser_tmdsclk_p(hdmi_ser_tmdsclk_p),
                             .hdmi_ser_tmdsclk_n(hdmi_ser_tmdsclk_n),
                             // Inputs
                             .clk_hdmi_fast     (clk_hdmi_fast),
                             .rst_hdmi_n        (rst_hdmi_n),
                             .hdmi_fifo_90bit_out(hdmi_fifo_90bit_out[89:0]),
                             .hdmi_synchronized_ready(hdmi_synchronized_ready));
endmodule // hdmi_top_module
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// "../../module/verilog"
// )
// End:
