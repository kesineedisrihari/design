module hdmi_fifo(/*AUTOARG*/
  // Outputs
  hdmi_fifo_90bit_out, hdmi_fifo_ready, hdmi_tm_enable,
  // Inputs
  clk_hdmi_slow, rst_hdmi_n, hdmi_tm_data_10bit_out_r0,
  hdmi_tm_data_10bit_out_r1, hdmi_tm_data_10bit_out_r2, hdmi_synchronized_ack,
  hdmi_tm_read_enable0, hdmi_tm_read_enable1, hdmi_tm_read_enable2
  );

  /*AUTOINPUT*/
  /*AUTOOUTPUT*/
 
  //HDMI FIFO INPUT
  input                clk_hdmi_slow;
  input                rst_hdmi_n; // The same as for the serializer
  input [9:0]          hdmi_tm_data_10bit_out_r0;
  input [9:0]          hdmi_tm_data_10bit_out_r1;
  input [9:0]          hdmi_tm_data_10bit_out_r2;
  input                hdmi_synchronized_ack;
  input                hdmi_tm_read_enable0;
  input                hdmi_tm_read_enable1;
  input                hdmi_tm_read_enable2;

  //HMDI FIFO OUTPUT
  output [89:0]        hdmi_fifo_90bit_out;
  output               hdmi_fifo_ready;
  output               hdmi_tm_enable;

 
  /*AUTOREG*/
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg [89:0]            hdmi_fifo_90bit_out;
  reg                   hdmi_fifo_ready;
  reg                   hdmi_tm_enable;
  // End of automatics
  
  /*AUOTWIRE*/
  
  //HDMI FIFO REGISTERS
  reg [9:0]            hdmi_fifo_ch0_1_r;
  reg [9:0]            hdmi_fifo_ch0_2_r;
  reg [9:0]            hdmi_fifo_ch0_3_r;
  reg [9:0]            hdmi_fifo_ch0_4_r;
  reg [9:0]            hdmi_fifo_ch0_5_r;
  reg [9:0]            hdmi_fifo_ch0_6_r;
  
  reg [9:0]            hdmi_fifo_ch1_1_r;
  reg [9:0]            hdmi_fifo_ch1_2_r;
  reg [9:0]            hdmi_fifo_ch1_3_r;
  reg [9:0]            hdmi_fifo_ch1_4_r;
  reg [9:0]            hdmi_fifo_ch1_5_r;
  reg [9:0]            hdmi_fifo_ch1_6_r;
  
  reg [9:0]            hdmi_fifo_ch2_1_r;
  reg [9:0]            hdmi_fifo_ch2_2_r;
  reg [9:0]            hdmi_fifo_ch2_3_r;
  reg [9:0]            hdmi_fifo_ch2_4_r;
  reg [9:0]            hdmi_fifo_ch2_5_r;
  reg [9:0]            hdmi_fifo_ch2_6_r;

  reg [2:0]            hdmi_fifo_counter_r;
  reg [2:0]            hdmi_fifo_current_pos_r;
  reg                  hdmi_fifo_load_nr_r;
  reg                  hdmi_fifo_read_enable;
  reg                  hdmi_fifo_init; 

  
  always @ (posedge clk_hdmi_slow or negedge rst_hdmi_n) begin

/*******************************************************************************************/
           // RESET
/*******************************************************************************************/

    if (rst_hdmi_n == 1'b0) begin
      hdmi_tm_enable <= 1'h1;
      hdmi_fifo_init <= 1'h1;

      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_fifo_90bit_out <= 90'h0;
      hdmi_fifo_ch0_1_r <= 10'h0;
      hdmi_fifo_ch0_2_r <= 10'h0;
      hdmi_fifo_ch0_3_r <= 10'h0;
      hdmi_fifo_ch0_4_r <= 10'h0;
      hdmi_fifo_ch0_5_r <= 10'h0;
      hdmi_fifo_ch0_6_r <= 10'h0;
      hdmi_fifo_ch1_1_r <= 10'h0;
      hdmi_fifo_ch1_2_r <= 10'h0;
      hdmi_fifo_ch1_3_r <= 10'h0;
      hdmi_fifo_ch1_4_r <= 10'h0;
      hdmi_fifo_ch1_5_r <= 10'h0;
      hdmi_fifo_ch1_6_r <= 10'h0;
      hdmi_fifo_ch2_1_r <= 10'h0;
      hdmi_fifo_ch2_2_r <= 10'h0;
      hdmi_fifo_ch2_3_r <= 10'h0;
      hdmi_fifo_ch2_4_r <= 10'h0;
      hdmi_fifo_ch2_5_r <= 10'h0;
      hdmi_fifo_ch2_6_r <= 10'h0;
      hdmi_fifo_counter_r <= 3'h0;
      hdmi_fifo_current_pos_r <= 3'h0;
      hdmi_fifo_load_nr_r <= 1'h0;
      hdmi_fifo_ready <= 1'h0;
      // End of automatics
    end
    else begin
/*******************************************************************************************/
           // FIFO CONTROL and READ
/*******************************************************************************************/
  //READING & POS UPDATE
      
      if(hdmi_fifo_read_enable == 1'b1) begin     
        case (hdmi_fifo_current_pos_r)   
          3'h1: begin           
            hdmi_fifo_ch0_1_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_1_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_1_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h2;
          end
          3'h2: begin
            hdmi_fifo_ch0_2_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_2_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_2_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h3;
          end
          3'h3: begin
            hdmi_fifo_ch0_3_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_3_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_3_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h4;
          end
          3'h4: begin
            hdmi_fifo_ch0_4_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_4_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_4_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h5;
          end
          3'h5: begin
            hdmi_fifo_ch0_5_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_5_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_5_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h6;
          end
          3'h6: begin
            hdmi_fifo_ch0_6_r <= hdmi_tm_data_10bit_out_r0;
            hdmi_fifo_ch1_6_r <= hdmi_tm_data_10bit_out_r1;
            hdmi_fifo_ch2_6_r <= hdmi_tm_data_10bit_out_r2;
            hdmi_fifo_current_pos_r <= 3'h1;
          end
          default: begin
            hdmi_fifo_current_pos_r <= 3'h1;
          end
        endcase // case (hdmi_fifo_current_pos)


        if (hdmi_synchronized_ack == 1'b1) begin
          hdmi_fifo_init <= 1'b1;
        end
        
 
  //TMDS DISABLE  
               
        if (hdmi_fifo_counter_r > 6)begin
          hdmi_fifo_counter_r <= 3'h0;
          hdmi_tm_enable <= 1'b0;
        end
        else if (hdmi_fifo_counter_r >= 4 && hdmi_synchronized_ack == 1'b0) begin
          hdmi_tm_enable <= 1'b0;
          hdmi_fifo_counter_r <= hdmi_fifo_counter_r + 1;
        end                           
        else begin
          hdmi_tm_enable <= 1'b1;
          hdmi_fifo_counter_r <= (hdmi_fifo_counter_r + 1);
        end
      end                             // 
        

/*******************************************************************************************/
                // SENDING DATA 
/*******************************************************************************************/

  //DATA OUTPUT TO SERIALIZER & COUNTER REDUCTION

      
      
      if((hdmi_fifo_init == 1'b1 || hdmi_synchronized_ack == 1'b1) && 
         ((hdmi_fifo_counter_r >= 3'h2 && hdmi_fifo_read_enable == 1'b1) || 
          (hdmi_fifo_counter_r >= 3'h3 && hdmi_fifo_read_enable == 1'b0))) begin
        
        case(hdmi_fifo_load_nr_r)  // 

          1'b0: begin
            hdmi_fifo_90bit_out[9:0]   <= hdmi_fifo_ch0_1_r;
            hdmi_fifo_90bit_out[19:10] <= hdmi_fifo_ch0_2_r;
            hdmi_fifo_90bit_out[29:20] <= hdmi_fifo_ch0_3_r;
            hdmi_fifo_90bit_out[39:30] <= hdmi_fifo_ch1_1_r;
            hdmi_fifo_90bit_out[49:40] <= hdmi_fifo_ch1_2_r;
            hdmi_fifo_90bit_out[59:50] <= hdmi_fifo_ch1_3_r;
            hdmi_fifo_90bit_out[69:60] <= hdmi_fifo_ch2_1_r;
            hdmi_fifo_90bit_out[79:70] <= hdmi_fifo_ch2_2_r;
            hdmi_fifo_90bit_out[89:80] <= hdmi_fifo_ch2_3_r;
            // 
            hdmi_fifo_ready            <= 1'b1;
            hdmi_fifo_init             <= 1'b0;
            hdmi_fifo_load_nr_r <= 1'b1;
            hdmi_tm_enable <= 1'b1; // Goes active since 3x3 register have been read

            
          end                        // 

          1'b1: begin                // 
            hdmi_fifo_90bit_out[9:0]   <= hdmi_fifo_ch0_4_r;
            hdmi_fifo_90bit_out[19:10] <= hdmi_fifo_ch0_5_r;
            hdmi_fifo_90bit_out[29:20] <= hdmi_fifo_ch0_6_r;
            hdmi_fifo_90bit_out[39:30] <= hdmi_fifo_ch1_4_r;
            hdmi_fifo_90bit_out[49:40] <= hdmi_fifo_ch1_5_r;
            hdmi_fifo_90bit_out[59:50] <= hdmi_fifo_ch1_6_r;
            hdmi_fifo_90bit_out[69:60] <= hdmi_fifo_ch2_4_r;
            hdmi_fifo_90bit_out[79:70] <= hdmi_fifo_ch2_5_r;
            hdmi_fifo_90bit_out[89:80] <= hdmi_fifo_ch2_6_r;
            // 
            hdmi_fifo_ready            <= 1'b1;
            hdmi_fifo_init             <= 1'b0;
            hdmi_fifo_load_nr_r <= 1'b0;
            hdmi_tm_enable <= 1'b1; // Goes active since 3x3 register have been read


          end                        // 

          default: begin             // 
            hdmi_fifo_90bit_out <= 90'h0;
            hdmi_fifo_ready     <= 1'b0; // If no data ready, then no ready signal
            hdmi_fifo_counter_r <= 3'h0; // Stays the same
            hdmi_fifo_init             <= 1'b0;
            hdmi_fifo_load_nr_r <= 1'b0;
          end                                          // 

        endcase                     // case (hdmi_fifo_load_nr_r)

        if(hdmi_fifo_read_enable == 1'b1) begin
          hdmi_fifo_counter_r        <= hdmi_fifo_counter_r - 2;
        end
        else begin
          hdmi_fifo_counter_r        <= hdmi_fifo_counter_r - 3;
        end                           
      end

/*******************************************************************************************/
      //TMDS RE-ENABLING
/*******************************************************************************************/

    end
  end                               // end if not reset
 
  always @ (*) begin
    hdmi_fifo_read_enable <= (hdmi_tm_read_enable0 && hdmi_tm_read_enable1 
                                && hdmi_tm_read_enable2);
    //Collecting 3 enable signals from the 3 TMDS-modules.
  end
  
endmodule                             
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// )
// End:
