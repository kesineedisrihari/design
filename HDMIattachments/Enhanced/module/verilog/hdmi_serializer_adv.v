module hdmi_serializer_adv(/*AUTOARG*/
  // Outputs
  hdmi_ser_ch0_out_p, hdmi_ser_ch1_out_p, hdmi_ser_ch2_out_p,
  hdmi_ser_ch0_out_n, hdmi_ser_ch1_out_n, hdmi_ser_ch2_out_n, hdmi_ser_ack,
  hdmi_ser_tmdsclk_p, hdmi_ser_tmdsclk_n,
  // Inputs
  clk_hdmi_fast, rst_hdmi_n, hdmi_fifo_90bit_out, hdmi_synchronized_ready
  );

  /*AUTOINPUT*/
  /*AUTOOUTPUT*/


  //HDMI SERIALIZER INPUT
  input                clk_hdmi_fast;
  input                rst_hdmi_n;
  input [89:0]         hdmi_fifo_90bit_out;
  input                hdmi_synchronized_ready;

  //HMDI SERIALIZER OUTPUT
  output               hdmi_ser_ch0_out_p;
  output               hdmi_ser_ch1_out_p;
  output               hdmi_ser_ch2_out_p;
  output               hdmi_ser_ch0_out_n;
  output               hdmi_ser_ch1_out_n;
  output               hdmi_ser_ch2_out_n;
  output               hdmi_ser_ack;
  output               hdmi_ser_tmdsclk_p;
  output               hdmi_ser_tmdsclk_n;
  
  /*AUTOREG*/
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg                   hdmi_ser_tmdsclk_n;
  reg                   hdmi_ser_tmdsclk_p;
  // End of automatics
  /*AUTOWIRE*/
  
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg                   hdmi_ser_ch0_out_n;
  reg                   hdmi_ser_ch0_out_p;
  reg                   hdmi_ser_ch1_out_n;
  reg                   hdmi_ser_ch1_out_p;
  reg                   hdmi_ser_ch2_out_n;
  reg                   hdmi_ser_ch2_out_p;
  reg                   hdmi_ser_ack;
  reg [14:0]            hdmi_ser_pos_counter;
  reg [4:0]             hdmi_ser_tmds_counter;
  reg                   hdmi_ser_ack_control_r;
  reg [2:0]             hdmi_ser_load;
  
  // End of automatics

  reg [30:0]            hdmi_ser_ch0_data;
  reg [30:0]            hdmi_ser_ch1_data;
  reg [30:0]            hdmi_ser_ch2_data;

  wire [89:0]           hdmi_fifo_90bit_out_r;
  wire [89:0]           hdmi_fifo_90bit_out;

  assign hdmi_fifo_90bit_out_r = hdmi_fifo_90bit_out;

  always @ (posedge hdmi_ser_tmds_counter[0]  or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      hdmi_ser_tmdsclk_p <= 1'h1;
      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_ser_tmdsclk_n <= 1'h0;
      // End of automatics
    end
    else begin
        hdmi_ser_tmdsclk_p <= ~hdmi_ser_tmdsclk_p;
        hdmi_ser_tmdsclk_n <= ~hdmi_ser_tmdsclk_n;
    end
  end
  
  always @ (posedge clk_hdmi_fast or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      hdmi_ser_pos_counter <= 15'h2000;
      hdmi_ser_tmds_counter <= 5'h10;

      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_ser_ack <= 1'h0;
      hdmi_ser_ack_control_r <= 1'h0;
      hdmi_ser_ch0_data <= 31'h0;
      hdmi_ser_ch0_out_n <= 1'h0;
      hdmi_ser_ch0_out_p <= 1'h0;
      hdmi_ser_ch1_data <= 31'h0;
      hdmi_ser_ch1_out_n <= 1'h0;
      hdmi_ser_ch1_out_p <= 1'h0;
      hdmi_ser_ch2_data <= 31'h0;
      hdmi_ser_ch2_out_n <= 1'h0;
      hdmi_ser_ch2_out_p <= 1'h0;
      hdmi_ser_load <= 3'h0;
      // End of automatics
    end
    else begin

/*******************************************************************************************/
      //RUNNING
/*******************************************************************************************/
      
      hdmi_ser_pos_counter[14:0] <= {hdmi_ser_pos_counter[0], hdmi_ser_pos_counter[14:1]};
      hdmi_ser_tmds_counter[4:0] <= {hdmi_ser_tmds_counter[0], hdmi_ser_tmds_counter[4:1]};

/*******************************************************************************************/
      hdmi_ser_ch0_out_n    <= ~hdmi_ser_ch0_data[0];
      hdmi_ser_ch0_out_p    <= hdmi_ser_ch0_data[0];
      
      if (hdmi_ser_load[0] == 1'b1) begin
        hdmi_ser_ch0_data[0]    <= hdmi_ser_ch0_data[1];
        hdmi_ser_ch0_data[30:1] <= hdmi_fifo_90bit_out_r[29:0];
        hdmi_ser_load[0]        <= 1'b0;
      end
      else        hdmi_ser_ch0_data <= hdmi_ser_ch0_data >> 1;
/*******************************************************************************************/
      hdmi_ser_ch1_out_n    <= ~hdmi_ser_ch1_data[0];
      hdmi_ser_ch1_out_p    <= hdmi_ser_ch1_data[0];

      if (hdmi_ser_load[1] == 1'b1) begin
        hdmi_ser_ch1_data[0]    <= hdmi_ser_ch1_data[1];
        hdmi_ser_ch1_data[30:1] <= hdmi_fifo_90bit_out_r[59:30];
        hdmi_ser_load[1]        <= 1'b0;
      end
      else        hdmi_ser_ch1_data <= hdmi_ser_ch1_data >> 1;
/*******************************************************************************************/
      hdmi_ser_ch2_out_n    <= ~hdmi_ser_ch2_data[0];
      hdmi_ser_ch2_out_p    <= hdmi_ser_ch2_data[0];
      if (hdmi_ser_load[2] == 1'b1) begin
        hdmi_ser_ch2_data[0]    <= hdmi_ser_ch2_data[1];
        hdmi_ser_ch2_data[30:1] <= hdmi_fifo_90bit_out_r[89:30];
        hdmi_ser_load[2]        <= 1'b0;
      end
      else        hdmi_ser_ch2_data <= hdmi_ser_ch2_data >> 1;
/*******************************************************************************************/


//-*******************************************************************************************-/
      //CONTROL      
//-*******************************************************************************************-/

      if (hdmi_synchronized_ready == 1'b1) begin
        if (hdmi_ser_pos_counter[0] == 1'b1) begin
          if (hdmi_ser_ack_control_r == 1'b1) begin
            hdmi_ser_ack           <= 1'b0;
            hdmi_ser_ack_control_r <= 1'b0;
          end
          else begin
            hdmi_ser_ack_control_r <= 1'b1;
            hdmi_ser_ack           <= 1'b1;
            hdmi_ser_load          <= 3'b111;
          end
        end // if (hdmi_ser_pos_counter[0] == 1'b1)
      end

    end // else: !if(rst_hdmi_n == 1'b0)
  end   // 
endmodule
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// )
// End:










                       
                             
/* -----\/----- EXCLUDED -----\/-----
      if (hdmi_ser_runflag == 1'b1) begin
        if (hdmi_ser_pos_counter[0] == 1'b1 && hdmi_ser_ack_control_r == 1'b0) begin   //
          hdmi_ser_ack_control_r <= 1'b1;
          if (hdmi_synchronized_ready == 1'b0) begin // Changed from rdy_r
            hdmi_ser_runflag <= 1'b0;
          end
          else begin
            hdmi_ser_runflag <= 1'b1;
            hdmi_ser_load <= 4'b1111;
         end
        end // 
        //         
        else begin
          if (hdmi_ser_pos_counter[0] == 1'b1 && hdmi_ser_ack_control_r == 1'b1) begin
            hdmi_ser_ack <= 1'b0;
            hdmi_ser_ack_control_r <= 1'b0;
          end
          hdmi_ser_runflag <= 1'b1;
        end 
      end // if (hdmi_ser_runflag == 1'b1)
      
/-*******************************************************************************************-/
      //IDLE      
/-*******************************************************************************************-/

      else begin
        if (hdmi_synchronized_ready == 1'b1) begin // changed from rdy_r
          hdmi_ser_runflag       <= 1'b1;
          hdmi_ser_pos_counter   <= 15'h1;
          hdmi_ser_tmds_counter  <= 5'h10;
          hdmi_ser_ack           <= 1'b1;
          hdmi_ser_ack_control_r <= 1'b0;
          hdmi_ser_load          <= 4'b1111;
          
        end
        else begin
          hdmi_ser_runflag       <= 1'b0;
          hdmi_ser_ack           <= 1'b1;
          hdmi_ser_ack_control_r <= 1'b0;
          hdmi_ser_load          <= 4'b0000;
          
        end
      end
 -----/\----- EXCLUDED -----/\----- */
