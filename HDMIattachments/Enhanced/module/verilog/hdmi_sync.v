module hdmi_sync(/*AUTOARG*/
  // Outputs
  hdmi_synchronized_ready, hdmi_synchronized_ack,
  // Inputs
  clk_hdmi_slow, clk_hdmi_fast, rst_hdmi_n, hdmi_fifo_ready, hdmi_ser_ack
  );

  /*AUTOINPUT*/
  /*AUTOOUTPUT*/

  input              clk_hdmi_slow;
  input              clk_hdmi_fast;
  input              rst_hdmi_n;
  input              hdmi_fifo_ready;
  input              hdmi_ser_ack;

  output             hdmi_synchronized_ready;
  output             hdmi_synchronized_ack;

 /*AUTOREG*/
  // Beginning of automatic regs (for this module's undeclared outputs)
  reg                   hdmi_synchronized_ack;
  reg                   hdmi_synchronized_ready;
  // End of automatics
  reg                hdmi_ready_input_dly1;
  reg                hdmi_ack_input_dly1;
  reg                hdmi_ack_input_dly2;
  reg                hdmi_ack_input_dly3;

  /*AUTOWIRE*/

  always @ (posedge clk_hdmi_slow or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      /*AUTORESET*/
      // Beginning of autoreset for uninitialized flops
      hdmi_ack_input_dly1 <= 1'h0;
      hdmi_ack_input_dly2 <= 1'h0;
      hdmi_ack_input_dly3 <= 1'h0;
      // End of automatics
    end
    else begin
      hdmi_ack_input_dly1    <= hdmi_ser_ack;
      hdmi_ack_input_dly2    <= hdmi_ack_input_dly1;
      hdmi_ack_input_dly3    <= hdmi_ack_input_dly2;
    end
  end
  
  always @ (posedge clk_hdmi_fast or negedge rst_hdmi_n) begin
    if (rst_hdmi_n == 1'b0) begin
      hdmi_ready_input_dly1   <= 1'h0;
      hdmi_synchronized_ready <= 1'h0;
    end
    else begin
      hdmi_ready_input_dly1   <= hdmi_fifo_ready;
      hdmi_synchronized_ready <= hdmi_ready_input_dly1;
    end
  end


  always @ (*) begin
    hdmi_synchronized_ack    = (hdmi_ack_input_dly2 && ~hdmi_ack_input_dly3);
  end                                 

endmodule
 
// Local Variables:
// verilog-library-directories:(
// "."
// "../../custom/verilog"
// )
// End:
