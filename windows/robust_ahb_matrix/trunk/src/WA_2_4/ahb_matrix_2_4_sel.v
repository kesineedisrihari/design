



module ahb_matrix_2_4_sel(clk,reset,S0_mstr,S1_mstr,S2_mstr,S3_mstr,S0_HREADY,S1_HREADY,S2_HREADY,S3_HREADY,S0_M0,S1_M0,S2_M0,S3_M0,S0_M1,S1_M1,S2_M1,S3_M1,S0_M0_resp,S1_M0_resp,S2_M0_resp,S3_M0_resp,S0_M1_resp,S1_M1_resp,S2_M1_resp,S3_M1_resp);

   input                clk;
   input                reset;

   input [2-1:0]           S0_mstr;
   input [2-1:0]           S1_mstr;
   input [2-1:0]           S2_mstr;
   input [2-1:0]           S3_mstr;
   
   input                       S0_HREADY;
   input                       S1_HREADY;
   input                       S2_HREADY;
   input                       S3_HREADY;
   
   
   output                S0_M0;
   output                S1_M0;
   output                S2_M0;
   output                S3_M0;
   output                S0_M1;
   output                S1_M1;
   output                S2_M1;
   output                S3_M1;
   output                      S0_M0_resp;
   output                      S1_M0_resp;
   output                      S2_M0_resp;
   output                      S3_M0_resp;
   output                      S0_M1_resp;
   output                      S1_M1_resp;
   output                      S2_M1_resp;
   output                      S3_M1_resp;
   


   
   reg [2-1:0]             S0_mstr_resp;
   reg [2-1:0]             S1_mstr_resp;
   reg [2-1:0]             S2_mstr_resp;
   reg [2-1:0]             S3_mstr_resp;


   always @(posedge clk or posedge reset)
     if (reset)
       S0_mstr_resp <= #1 {2{1'b0}};
     else if (S0_HREADY)
       S0_mstr_resp <= #1 S0_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S1_mstr_resp <= #1 {2{1'b0}};
     else if (S1_HREADY)
       S1_mstr_resp <= #1 S1_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S2_mstr_resp <= #1 {2{1'b0}};
     else if (S2_HREADY)
       S2_mstr_resp <= #1 S2_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S3_mstr_resp <= #1 {2{1'b0}};
     else if (S3_HREADY)
       S3_mstr_resp <= #1 S3_mstr;


     
   assign               S0_M0      = S0_mstr[0];
   assign               S1_M0      = S1_mstr[0];
   assign               S2_M0      = S2_mstr[0];
   assign               S3_M0      = S3_mstr[0];
   assign               S0_M1      = S0_mstr[1];
   assign               S1_M1      = S1_mstr[1];
   assign               S2_M1      = S2_mstr[1];
   assign               S3_M1      = S3_mstr[1];
   
   assign               S0_M0_resp = S0_mstr_resp[0];
   assign               S1_M0_resp = S1_mstr_resp[0];
   assign               S2_M0_resp = S2_mstr_resp[0];
   assign               S3_M0_resp = S3_mstr_resp[0];
   assign               S0_M1_resp = S0_mstr_resp[1];
   assign               S1_M1_resp = S1_mstr_resp[1];
   assign               S2_M1_resp = S2_mstr_resp[1];
   assign               S3_M1_resp = S3_mstr_resp[1];
   
endmodule






