




  
module prgen_arbiter_mstr_2_4(clk,reset,M_last,M_req,M_grant,M0_slave,M1_slave,S0_master,S1_master,S2_master,S3_master);

   input                   clk;
   input                   reset;

   input [2-1:0]           M_last;
   input [2-1:0]           M_req;
   input [2-1:0]           M_grant;
   
   input [2-1:0]           M0_slave;
   input [2-1:0]           M1_slave;
   
   output [2-1:0]           S0_master;
   output [2-1:0]           S1_master;
   output [2-1:0]           S2_master;
   output [2-1:0]           S3_master;


   
   reg [2:0]               S0_master_prio_reg;
   reg [2:0]               S1_master_prio_reg;
   reg [2:0]               S2_master_prio_reg;
   reg [2:0]               S3_master_prio_reg;
   wire [2-1:0]               S0_master_prio;
   wire [2-1:0]               S1_master_prio;
   wire [2-1:0]               S2_master_prio;
   wire [2-1:0]               S3_master_prio;
   reg [2-1:0]               S0_master_d;
   reg [2-1:0]               S1_master_d;
   reg [2-1:0]               S2_master_d;
   reg [2-1:0]               S3_master_d;
   
   wire [2-1:0]               M_S0;
   wire [2-1:0]               M_S1;
   wire [2-1:0]               M_S2;
   wire [2-1:0]               M_S3;
   wire [2-1:0]               M_S0_valid;
   wire [2-1:0]               M_S1_valid;
   wire [2-1:0]               M_S2_valid;
   wire [2-1:0]               M_S3_valid;
   wire [2-1:0]               M_S0_prio;
   wire [2-1:0]               M_S1_prio;
   wire [2-1:0]               M_S2_prio;
   wire [2-1:0]               M_S3_prio;
   reg [2-1:0]               M_S0_burst;
   reg [2-1:0]               M_S1_burst;
   reg [2-1:0]               M_S2_burst;
   reg [2-1:0]               M_S3_burst;


   
   
   parameter                   MASTER_NONE = 2'b00;
   parameter                   MASTER0    = 2'b01;
   parameter                   MASTER1    = 2'b10;
   
   
   

   always @(posedge clk or posedge reset)
     if (reset)
       begin
      S0_master_prio_reg[2:1] <= #1 {2{1'b0}};
      S1_master_prio_reg[2:1] <= #1 {2{1'b0}};
      S2_master_prio_reg[2:1] <= #1 {2{1'b0}};
      S3_master_prio_reg[2:1] <= #1 {2{1'b0}};
      S0_master_prio_reg[0]          <= #1 1'b1;
      S1_master_prio_reg[0]          <= #1 1'b1;
      S2_master_prio_reg[0]          <= #1 1'b1;
      S3_master_prio_reg[0]          <= #1 1'b1;
       end
     else if (|(M_req & M_grant & M_last))
       begin      
      S0_master_prio_reg[2:1] <= #1 S0_master_prio_reg[2-1:0];
      S1_master_prio_reg[2:1] <= #1 S1_master_prio_reg[2-1:0];
      S2_master_prio_reg[2:1] <= #1 S2_master_prio_reg[2-1:0];
      S3_master_prio_reg[2:1] <= #1 S3_master_prio_reg[2-1:0];
      S0_master_prio_reg[0]          <= #1 S0_master_prio_reg[2-1];
      S1_master_prio_reg[0]          <= #1 S1_master_prio_reg[2-1];
      S2_master_prio_reg[0]          <= #1 S2_master_prio_reg[2-1];
      S3_master_prio_reg[0]          <= #1 S3_master_prio_reg[2-1];
       end

   assign S0_master_prio = S0_master_prio_reg[2-1:0];
   assign S1_master_prio = S1_master_prio_reg[2-1:0];
   assign S2_master_prio = S2_master_prio_reg[2-1:0];
   assign S3_master_prio = S3_master_prio_reg[2-1:0];
   
   assign M_S0_prio      = M_S0_valid & S0_master_prio;
   assign M_S1_prio      = M_S1_valid & S1_master_prio;
   assign M_S2_prio      = M_S2_valid & S2_master_prio;
   assign M_S3_prio      = M_S3_valid & S3_master_prio;
   


   always @(posedge clk or posedge reset)
     if (reset)
       begin
      S0_master_d <= #1 {2{1'b0}};
      S1_master_d <= #1 {2{1'b0}};
      S2_master_d <= #1 {2{1'b0}};
      S3_master_d <= #1 {2{1'b0}};
       end
     else
       begin
      S0_master_d <= #1 S0_master;
      S1_master_d <= #1 S1_master;
      S2_master_d <= #1 S2_master;
      S3_master_d <= #1 S3_master;
       end

     always @(posedge clk or posedge reset)                        
       if (reset)                                                  
     begin                                                     
        M_S0_burst[0] <= #1 1'b0;                        
        M_S1_burst[0] <= #1 1'b0;                        
        M_S2_burst[0] <= #1 1'b0;                        
        M_S3_burst[0] <= #1 1'b0;                        
     end                                                       
       else if (M_req[0])                         
     begin                                                     
        M_S0_burst[0] <= #1 S0_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S1_burst[0] <= #1 S1_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S2_burst[0] <= #1 S2_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S3_burst[0] <= #1 S3_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
     end
   
     always @(posedge clk or posedge reset)                        
       if (reset)                                                  
     begin                                                     
        M_S0_burst[1] <= #1 1'b0;                        
        M_S1_burst[1] <= #1 1'b0;                        
        M_S2_burst[1] <= #1 1'b0;                        
        M_S3_burst[1] <= #1 1'b0;                        
     end                                                       
       else if (M_req[1])                         
     begin                                                     
        M_S0_burst[1] <= #1 S0_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S1_burst[1] <= #1 S1_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S2_burst[1] <= #1 S2_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S3_burst[1] <= #1 S3_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
     end
   
      
     assign                              M_S0 = {M1_slave == 'd0 , M0_slave == 'd0};
     assign                              M_S1 = {M1_slave == 'd1 , M0_slave == 'd1};
     assign                              M_S2 = {M1_slave == 'd2 , M0_slave == 'd2};
     assign                              M_S3 = {M1_slave == 'd3 , M0_slave == 'd3};
   
   assign                  M_S0_valid = M_S0 & M_req;
   assign                  M_S1_valid = M_S1 & M_req;
   assign                  M_S2_valid = M_S2 & M_req;
   assign                  M_S3_valid = M_S3 & M_req;
   
   
     assign                    S0_master = 
                               |M_S0_burst ? S0_master_d : 
                                            M_S0_prio[0]  ? MASTER0 :  
                                            M_S0_prio[1]  ? MASTER1 :  
                            M_S0_valid[0] ? MASTER0 :      
                            M_S0_valid[1] ? MASTER1 :      
                            MASTER_NONE;
   
     assign                    S1_master = 
                               |M_S1_burst ? S1_master_d : 
                                            M_S1_prio[0]  ? MASTER0 :  
                                            M_S1_prio[1]  ? MASTER1 :  
                            M_S1_valid[0] ? MASTER0 :      
                            M_S1_valid[1] ? MASTER1 :      
                            MASTER_NONE;
   
     assign                    S2_master = 
                               |M_S2_burst ? S2_master_d : 
                                            M_S2_prio[0]  ? MASTER0 :  
                                            M_S2_prio[1]  ? MASTER1 :  
                            M_S2_valid[0] ? MASTER0 :      
                            M_S2_valid[1] ? MASTER1 :      
                            MASTER_NONE;
   
     assign                    S3_master = 
                               |M_S3_burst ? S3_master_d : 
                                            M_S3_prio[0]  ? MASTER0 :  
                                            M_S3_prio[1]  ? MASTER1 :  
                            M_S3_valid[0] ? MASTER0 :      
                            M_S3_valid[1] ? MASTER1 :      
                            MASTER_NONE;
   
      
     endmodule



