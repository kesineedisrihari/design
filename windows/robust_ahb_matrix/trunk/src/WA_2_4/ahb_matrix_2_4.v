



  
module ahb_matrix_2_4(clk,reset,M0_HADDR,M0_HBURST,M0_HSIZE,M0_HTRANS,M0_HWRITE,M0_HWDATA,M0_HRDATA,M0_HRESP,M0_HREADY,M1_HADDR,M1_HBURST,M1_HSIZE,M1_HTRANS,M1_HWRITE,M1_HWDATA,M1_HRDATA,M1_HRESP,M1_HREADY,S0_HADDR,S0_HBURST,S0_HSIZE,S0_HTRANS,S0_HWRITE,S0_HWDATA,S0_HRDATA,S0_HRESP,S0_HREADY,S1_HADDR,S1_HBURST,S1_HSIZE,S1_HTRANS,S1_HWRITE,S1_HWDATA,S1_HRDATA,S1_HRESP,S1_HREADY,S2_HADDR,S2_HBURST,S2_HSIZE,S2_HTRANS,S2_HWRITE,S2_HWDATA,S2_HRDATA,S2_HRESP,S2_HREADY,S3_HADDR,S3_HBURST,S3_HSIZE,S3_HTRANS,S3_HWRITE,S3_HWDATA,S3_HRDATA,S3_HRESP,S3_HREADY);

   input                clk;
   input                       reset;
   
   input [31:0]                M0_HADDR;
   input [2:0]                 M0_HBURST;
   input [1:0]                 M0_HSIZE;
   input [1:0]                 M0_HTRANS;
   input                       M0_HWRITE;
   input [31:0]                M0_HWDATA;
   output [31:0]               M0_HRDATA;
   output                      M0_HRESP;
   output                      M0_HREADY;
   
   input [31:0]                M1_HADDR;
   input [2:0]                 M1_HBURST;
   input [1:0]                 M1_HSIZE;
   input [1:0]                 M1_HTRANS;
   input                       M1_HWRITE;
   input [31:0]                M1_HWDATA;
   output [31:0]               M1_HRDATA;
   output                      M1_HRESP;
   output                      M1_HREADY;
   
   output [31:0]               S0_HADDR;
   output [2:0]                S0_HBURST;
   output [1:0]                S0_HSIZE;
   output [1:0]                S0_HTRANS;
   output                      S0_HWRITE;
   output [31:0]               S0_HWDATA;
   input [31:0]                S0_HRDATA;
   input                       S0_HRESP;
   input                       S0_HREADY;
   output [31:0]               S1_HADDR;
   output [2:0]                S1_HBURST;
   output [1:0]                S1_HSIZE;
   output [1:0]                S1_HTRANS;
   output                      S1_HWRITE;
   output [31:0]               S1_HWDATA;
   input [31:0]                S1_HRDATA;
   input                       S1_HRESP;
   input                       S1_HREADY;
   output [31:0]               S2_HADDR;
   output [2:0]                S2_HBURST;
   output [1:0]                S2_HSIZE;
   output [1:0]                S2_HTRANS;
   output                      S2_HWRITE;
   output [31:0]               S2_HWDATA;
   input [31:0]                S2_HRDATA;
   input                       S2_HRESP;
   input                       S2_HREADY;
   output [31:0]               S3_HADDR;
   output [2:0]                S3_HBURST;
   output [1:0]                S3_HSIZE;
   output [1:0]                S3_HTRANS;
   output                      S3_HWRITE;
   output [31:0]               S3_HWDATA;
   input [31:0]                S3_HRDATA;
   input                       S3_HRESP;
   input                       S3_HREADY;

   

   wire [2-1:0]            S0_mstr;
   wire [2-1:0]            S1_mstr;
   wire [2-1:0]            S2_mstr;
   wire [2-1:0]            S3_mstr;
   
   wire [2-1:0]         M0_slv;
   wire [2-1:0]         M1_slv;

   wire                        M0_HLAST;
   wire                        M1_HLAST;
   
   wire                        S0_M0;
   wire                        S1_M0;
   wire                        S2_M0;
   wire                        S3_M0;
   wire                        S0_M1;
   wire                        S1_M1;
   wire                        S2_M1;
   wire                        S3_M1;
   wire                        S0_M0_resp;
   wire                        S1_M0_resp;
   wire                        S2_M0_resp;
   wire                        S3_M0_resp;
   wire                        S0_M1_resp;
   wire                        S1_M1_resp;
   wire                        S2_M1_resp;
   wire                        S3_M1_resp;


   
   ahb_matrix_2_4_dec
   ahb_matrix_2_4_dec (
                  .M0_HADDR(M0_HADDR),
                  .M1_HADDR(M1_HADDR),
                  .M0_slv(M0_slv),
                  .M1_slv(M1_slv)
                  );


     ahb_matrix_2_4_hlast
       ahb_matrix_2_4_hlast(
                        .clk(clk),
                        .reset(reset),
                        .M0_HTRANS(M0_HTRANS),
                        .M1_HTRANS(M1_HTRANS),
                        .M0_HREADY(M0_HREADY),
                        .M1_HREADY(M1_HREADY),
                        .M0_HBURST(M0_HBURST),
                        .M1_HBURST(M1_HBURST),
                        .M0_HLAST(M0_HLAST),
                        .M1_HLAST(M1_HLAST)
                        );
   
   
   prgen_arbiter_mstr_2_4
   prgen_arbiter_mstr_2_4(
                                 .clk(clk),
                                 .reset(reset),
      
                                 .M0_slave(M0_slv),
                                 .M1_slave(M1_slv),
                                 
                                 .S0_master(S0_mstr),
                                 .S1_master(S1_mstr),
                                 .S2_master(S2_mstr),
                                 .S3_master(S3_mstr),
                                 
                                 .M_last({M1_HLAST , M0_HLAST}),
                                 .M_req({M1_HTRANS[1] , M0_HTRANS[1]}),
                                 .M_grant({M1_HREADY , M0_HREADY})
                                 );
   

   ahb_matrix_2_4_sel  
     ahb_matrix_2_4_sel (
             .clk(clk),
             .reset(reset),
             .S0_mstr(S0_mstr),
             .S1_mstr(S1_mstr),
             .S2_mstr(S2_mstr),
             .S3_mstr(S3_mstr),
             .S0_HREADY(S0_HREADY),
             .S1_HREADY(S1_HREADY),
             .S2_HREADY(S2_HREADY),
             .S3_HREADY(S3_HREADY),
             .S0_M0(S0_M0),
             .S1_M0(S1_M0),
             .S2_M0(S2_M0),
             .S3_M0(S3_M0),
             .S0_M1(S0_M1),
             .S1_M1(S1_M1),
             .S2_M1(S2_M1),
             .S3_M1(S3_M1),
             .S0_M0_resp(S0_M0_resp),
             .S1_M0_resp(S1_M0_resp),
             .S2_M0_resp(S2_M0_resp),
             .S3_M0_resp(S3_M0_resp),
             .S0_M1_resp(S0_M1_resp),
             .S1_M1_resp(S1_M1_resp),
             .S2_M1_resp(S2_M1_resp),
             .S3_M1_resp(S3_M1_resp)
             );

   
   ahb_matrix_2_4_bus  
     ahb_matrix_2_4_bus (
             .clk(clk),
             .reset(reset),
                     .M0_HADDR(M0_HADDR),
                     .M0_HBURST(M0_HBURST),
                     .M0_HSIZE(M0_HSIZE),
                     .M0_HTRANS(M0_HTRANS),
                     .M0_HWRITE(M0_HWRITE),
                     .M0_HWDATA(M0_HWDATA),
                     .M0_HRDATA(M0_HRDATA),
                     .M0_HRESP(M0_HRESP),
                     .M0_HREADY(M0_HREADY),
                     .M1_HADDR(M1_HADDR),
                     .M1_HBURST(M1_HBURST),
                     .M1_HSIZE(M1_HSIZE),
                     .M1_HTRANS(M1_HTRANS),
                     .M1_HWRITE(M1_HWRITE),
                     .M1_HWDATA(M1_HWDATA),
                     .M1_HRDATA(M1_HRDATA),
                     .M1_HRESP(M1_HRESP),
                     .M1_HREADY(M1_HREADY),
                     .S0_HADDR(S0_HADDR),
                     .S0_HBURST(S0_HBURST),
                     .S0_HSIZE(S0_HSIZE),
                     .S0_HTRANS(S0_HTRANS),
                     .S0_HWRITE(S0_HWRITE),
                     .S0_HWDATA(S0_HWDATA),
                     .S0_HRDATA(S0_HRDATA),
                     .S0_HRESP(S0_HRESP),
                     .S0_HREADY(S0_HREADY),
                     .S1_HADDR(S1_HADDR),
                     .S1_HBURST(S1_HBURST),
                     .S1_HSIZE(S1_HSIZE),
                     .S1_HTRANS(S1_HTRANS),
                     .S1_HWRITE(S1_HWRITE),
                     .S1_HWDATA(S1_HWDATA),
                     .S1_HRDATA(S1_HRDATA),
                     .S1_HRESP(S1_HRESP),
                     .S1_HREADY(S1_HREADY),
                     .S2_HADDR(S2_HADDR),
                     .S2_HBURST(S2_HBURST),
                     .S2_HSIZE(S2_HSIZE),
                     .S2_HTRANS(S2_HTRANS),
                     .S2_HWRITE(S2_HWRITE),
                     .S2_HWDATA(S2_HWDATA),
                     .S2_HRDATA(S2_HRDATA),
                     .S2_HRESP(S2_HRESP),
                     .S2_HREADY(S2_HREADY),
                     .S3_HADDR(S3_HADDR),
                     .S3_HBURST(S3_HBURST),
                     .S3_HSIZE(S3_HSIZE),
                     .S3_HTRANS(S3_HTRANS),
                     .S3_HWRITE(S3_HWRITE),
                     .S3_HWDATA(S3_HWDATA),
                     .S3_HRDATA(S3_HRDATA),
                     .S3_HRESP(S3_HRESP),
                     .S3_HREADY(S3_HREADY),
             .S0_M0(S0_M0),
             .S1_M0(S1_M0),
             .S2_M0(S2_M0),
             .S3_M0(S3_M0),
             .S0_M1(S0_M1),
             .S1_M1(S1_M1),
             .S2_M1(S2_M1),
             .S3_M1(S3_M1),
             .S0_M0_resp(S0_M0_resp),
             .S1_M0_resp(S1_M0_resp),
             .S2_M0_resp(S2_M0_resp),
             .S3_M0_resp(S3_M0_resp),
             .S0_M1_resp(S0_M1_resp),
             .S1_M1_resp(S1_M1_resp),
             .S2_M1_resp(S2_M1_resp),
             .S3_M1_resp(S3_M1_resp)
             );
   

   
  
endmodule






