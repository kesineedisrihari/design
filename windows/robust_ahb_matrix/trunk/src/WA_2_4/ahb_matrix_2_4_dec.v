




module ahb_matrix_2_4_dec (M0_HADDR,M1_HADDR,M0_slv,M1_slv);

   input [32-1:0]               M0_HADDR;
   input [32-1:0]               M1_HADDR;
   output [2-1:0]               M0_slv;
   output [2-1:0]               M1_slv;
   
   
   parameter                                  DEC_MSB =  32 - 1;
   parameter                                  DEC_LSB =  32 - 2;
   
   reg [2-1:0]                   M0_slv;
   reg [2-1:0]                   M1_slv;
   
     always @(*)
       begin                                                  
      case (M0_HADDR[DEC_MSB:DEC_LSB])    
        2'b00 : M0_slv = 'd0;  
        2'b01 : M0_slv = 'd1;  
        2'b10 : M0_slv = 'd2;  
        2'b11 : M0_slv = 'd3;  

            default : M0_slv = 'd3;                     
      endcase                                             
       end                                                    

     always @(*)
       begin                                                  
      case (M1_HADDR[DEC_MSB:DEC_LSB])    
        2'b00 : M1_slv = 'd0;  
        2'b01 : M1_slv = 'd1;  
        2'b10 : M1_slv = 'd2;  
        2'b11 : M1_slv = 'd3;  

            default : M1_slv = 'd3;                     
      endcase                                             
       end                                                    

      
     endmodule





