




module ahb_matrix_2_4_hlast (clk,reset,M0_HTRANS,M1_HTRANS,M0_HREADY,M1_HREADY,M0_HBURST,M1_HBURST,M0_HLAST,M1_HLAST);

   input                      clk;
   input                      reset;
   
   input [1:0]               M0_HTRANS;
   input [1:0]               M1_HTRANS;
   input                M0_HREADY;
   input                M1_HREADY;
   input [2:0]               M0_HBURST;
   input [2:0]               M1_HBURST;
   output                     M0_HLAST;
   output                     M1_HLAST;

   
   parameter                  TRANS_IDLE   = 2'b00;
   parameter                  TRANS_BUSY   = 2'b01;
   parameter                  TRANS_NONSEQ = 2'b10;
   parameter                  TRANS_SEQ    = 2'b11;

   parameter                  BURST_SINGLE = 3'b000;
   parameter                  BURST_INCR4  = 3'b011;
   parameter                  BURST_INCR8  = 3'b101;
   parameter                  BURST_INCR16 = 3'b111;


   reg [3:0]                  M0_count;
   reg [3:0]                  M1_count;


   assign                     M0_HLAST = (M0_count == 'd1) | ((M0_HTRANS == TRANS_NONSEQ) & M0_HREADY & (M0_HBURST == BURST_SINGLE));
   assign                     M1_HLAST = (M1_count == 'd1) | ((M1_HTRANS == TRANS_NONSEQ) & M1_HREADY & (M1_HBURST == BURST_SINGLE));

   always @(posedge clk or posedge reset)
     if (reset)
       M0_count <= #1 4'd15;
     else if ((M0_HTRANS == TRANS_NONSEQ) & M0_HREADY)
       M0_count <= #1 
                          (M0_HBURST == BURST_INCR4) ? 4'd3 :
                          (M0_HBURST == BURST_INCR8) ? 4'd7 :
                          (M0_HBURST == BURST_INCR16) ? 4'd15 :
                          4'd0;
     else if ((M0_HTRANS == TRANS_SEQ) & M0_HREADY)
       M0_count <= #1 M0_count - 1'b1;

   always @(posedge clk or posedge reset)
     if (reset)
       M1_count <= #1 4'd15;
     else if ((M1_HTRANS == TRANS_NONSEQ) & M1_HREADY)
       M1_count <= #1 
                          (M1_HBURST == BURST_INCR4) ? 4'd3 :
                          (M1_HBURST == BURST_INCR8) ? 4'd7 :
                          (M1_HBURST == BURST_INCR16) ? 4'd15 :
                          4'd0;
     else if ((M1_HTRANS == TRANS_SEQ) & M1_HREADY)
       M1_count <= #1 M1_count - 1'b1;


  
     endmodule





