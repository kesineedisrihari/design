



  
module ahb_matrix_2_4_bus(clk,reset,M0_HADDR,M0_HBURST,M0_HSIZE,M0_HTRANS,M0_HWRITE,M0_HWDATA,M0_HRDATA,M0_HRESP,M0_HREADY,M1_HADDR,M1_HBURST,M1_HSIZE,M1_HTRANS,M1_HWRITE,M1_HWDATA,M1_HRDATA,M1_HRESP,M1_HREADY,S0_HADDR,S0_HBURST,S0_HSIZE,S0_HTRANS,S0_HWRITE,S0_HWDATA,S0_HRDATA,S0_HRESP,S0_HREADY,S1_HADDR,S1_HBURST,S1_HSIZE,S1_HTRANS,S1_HWRITE,S1_HWDATA,S1_HRDATA,S1_HRESP,S1_HREADY,S2_HADDR,S2_HBURST,S2_HSIZE,S2_HTRANS,S2_HWRITE,S2_HWDATA,S2_HRDATA,S2_HRESP,S2_HREADY,S3_HADDR,S3_HBURST,S3_HSIZE,S3_HTRANS,S3_HWRITE,S3_HWDATA,S3_HRDATA,S3_HRESP,S3_HREADY,S0_M0,S1_M0,S2_M0,S3_M0,S0_M1,S1_M1,S2_M1,S3_M1,S0_M0_resp,S1_M0_resp,S2_M0_resp,S3_M0_resp,S0_M1_resp,S1_M1_resp,S2_M1_resp,S3_M1_resp);

   input                clk;
   input                       reset;
   
   input [31:0]                M0_HADDR;
   input [2:0]                 M0_HBURST;
   input [1:0]                 M0_HSIZE;
   input [1:0]                 M0_HTRANS;
   input                       M0_HWRITE;
   input [31:0]                M0_HWDATA;
   output [31:0]               M0_HRDATA;
   output                      M0_HRESP;
   output                      M0_HREADY;
   input [31:0]                M1_HADDR;
   input [2:0]                 M1_HBURST;
   input [1:0]                 M1_HSIZE;
   input [1:0]                 M1_HTRANS;
   input                       M1_HWRITE;
   input [31:0]                M1_HWDATA;
   output [31:0]               M1_HRDATA;
   output                      M1_HRESP;
   output                      M1_HREADY;
   output [31:0]               S0_HADDR;
   output [2:0]                S0_HBURST;
   output [1:0]                S0_HSIZE;
   output [1:0]                S0_HTRANS;
   output                      S0_HWRITE;
   output [31:0]               S0_HWDATA;
   input [31:0]                S0_HRDATA;
   input                       S0_HRESP;
   input                       S0_HREADY;
   output [31:0]               S1_HADDR;
   output [2:0]                S1_HBURST;
   output [1:0]                S1_HSIZE;
   output [1:0]                S1_HTRANS;
   output                      S1_HWRITE;
   output [31:0]               S1_HWDATA;
   input [31:0]                S1_HRDATA;
   input                       S1_HRESP;
   input                       S1_HREADY;
   output [31:0]               S2_HADDR;
   output [2:0]                S2_HBURST;
   output [1:0]                S2_HSIZE;
   output [1:0]                S2_HTRANS;
   output                      S2_HWRITE;
   output [31:0]               S2_HWDATA;
   input [31:0]                S2_HRDATA;
   input                       S2_HRESP;
   input                       S2_HREADY;
   output [31:0]               S3_HADDR;
   output [2:0]                S3_HBURST;
   output [1:0]                S3_HSIZE;
   output [1:0]                S3_HTRANS;
   output                      S3_HWRITE;
   output [31:0]               S3_HWDATA;
   input [31:0]                S3_HRDATA;
   input                       S3_HRESP;
   input                       S3_HREADY;

   input                       S0_M0;
   input                       S1_M0;
   input                       S2_M0;
   input                       S3_M0;
   input                       S0_M1;
   input                       S1_M1;
   input                       S2_M1;
   input                       S3_M1;
   input                       S0_M0_resp;
   input                       S1_M0_resp;
   input                       S2_M0_resp;
   input                       S3_M0_resp;
   input                       S0_M1_resp;
   input                       S1_M1_resp;
   input                       S2_M1_resp;
   input                       S3_M1_resp;
   

   parameter                   BUS_WIDTH = 32 + 3 + 2 + 2 + 1 + 32;

   wire [BUS_WIDTH-1:0]        S0_BUS;
   wire [BUS_WIDTH-1:0]        S1_BUS;
   wire [BUS_WIDTH-1:0]        S2_BUS;
   wire [BUS_WIDTH-1:0]        S3_BUS;
   
   wire [BUS_WIDTH-1:0]        M0_BUS;
   wire [BUS_WIDTH-1:0]        M1_BUS;
   

   
   assign                      M0_BUS = {M0_HADDR , M0_HBURST , M0_HSIZE , M0_HTRANS , M0_HWRITE};
   assign                      M1_BUS = {M1_HADDR , M1_HBURST , M1_HSIZE , M1_HTRANS , M1_HWRITE};
   
   assign                      S0_BUS = (M1_BUS & {BUS_WIDTH{S0_M1}}) | (M0_BUS & {BUS_WIDTH{S0_M0}});
   assign                      S1_BUS = (M1_BUS & {BUS_WIDTH{S1_M1}}) | (M0_BUS & {BUS_WIDTH{S1_M0}});
   assign                      S2_BUS = (M1_BUS & {BUS_WIDTH{S2_M1}}) | (M0_BUS & {BUS_WIDTH{S2_M0}});
   assign                      S3_BUS = (M1_BUS & {BUS_WIDTH{S3_M1}}) | (M0_BUS & {BUS_WIDTH{S3_M0}});
   
   assign                      {S0_HADDR , S0_HBURST , S0_HSIZE , S0_HTRANS , S0_HWRITE} = S0_BUS;
   assign                      {S1_HADDR , S1_HBURST , S1_HSIZE , S1_HTRANS , S1_HWRITE} = S1_BUS;
   assign                      {S2_HADDR , S2_HBURST , S2_HSIZE , S2_HTRANS , S2_HWRITE} = S2_BUS;
   assign                      {S3_HADDR , S3_HBURST , S3_HSIZE , S3_HTRANS , S3_HWRITE} = S3_BUS;

   assign                      S0_HWDATA =  (M1_HWDATA & {32{S0_M1_resp}}) | (M0_HWDATA & {32{S0_M0_resp}});                    
   assign                      S1_HWDATA =  (M1_HWDATA & {32{S1_M1_resp}}) | (M0_HWDATA & {32{S1_M0_resp}});                    
   assign                      S2_HWDATA =  (M1_HWDATA & {32{S2_M1_resp}}) | (M0_HWDATA & {32{S2_M0_resp}});                    
   assign                      S3_HWDATA =  (M1_HWDATA & {32{S3_M1_resp}}) | (M0_HWDATA & {32{S3_M0_resp}});                    
   
   assign                      M0_HRDATA = ({32{S3_M0_resp}} & S3_HRDATA) | ({32{S2_M0_resp}} & S2_HRDATA) | ({32{S1_M0_resp}} & S1_HRDATA) | ({32{S0_M0_resp}} & S0_HRDATA);
   assign                      M0_HRESP = ({1{S3_M0_resp}} & S3_HRESP) | ({1{S2_M0_resp}} & S2_HRESP) | ({1{S1_M0_resp}} & S1_HRESP) | ({1{S0_M0_resp}} & S0_HRESP);
   
   assign                      M0_HREADY = ((S3_M0|S3_M0_resp)&S3_HREADY) | ((S2_M0|S2_M0_resp)&S2_HREADY) | ((S1_M0|S1_M0_resp)&S1_HREADY) | ((S0_M0|S0_M0_resp)&S0_HREADY);
   
   assign                      M1_HRDATA = ({32{S3_M1_resp}} & S3_HRDATA) | ({32{S2_M1_resp}} & S2_HRDATA) | ({32{S1_M1_resp}} & S1_HRDATA) | ({32{S0_M1_resp}} & S0_HRDATA);
   assign                      M1_HRESP = ({1{S3_M1_resp}} & S3_HRESP) | ({1{S2_M1_resp}} & S2_HRESP) | ({1{S1_M1_resp}} & S1_HRESP) | ({1{S0_M1_resp}} & S0_HRESP);
   
   assign                      M1_HREADY = ((S3_M1|S3_M1_resp)&S3_HREADY) | ((S2_M1|S2_M1_resp)&S2_HREADY) | ((S1_M1|S1_M1_resp)&S1_HREADY) | ((S0_M1|S0_M1_resp)&S0_HREADY);
   
  
  
endmodule






