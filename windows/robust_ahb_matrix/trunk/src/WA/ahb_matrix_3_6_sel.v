



module ahb_matrix_3_6_sel(clk,reset,S0_mstr,S1_mstr,S2_mstr,S3_mstr,S4_mstr,S5_mstr,S0_HREADY,S1_HREADY,S2_HREADY,S3_HREADY,S4_HREADY,S5_HREADY,S0_M0,S1_M0,S2_M0,S3_M0,S4_M0,S5_M0,S0_M1,S1_M1,S2_M1,S3_M1,S4_M1,S5_M1,S0_M2,S1_M2,S2_M2,S3_M2,S4_M2,S5_M2,S0_M0_resp,S1_M0_resp,S2_M0_resp,S3_M0_resp,S4_M0_resp,S5_M0_resp,S0_M1_resp,S1_M1_resp,S2_M1_resp,S3_M1_resp,S4_M1_resp,S5_M1_resp,S0_M2_resp,S1_M2_resp,S2_M2_resp,S3_M2_resp,S4_M2_resp,S5_M2_resp);

   input                clk;
   input                reset;

   input [3-1:0]           S0_mstr;
   input [3-1:0]           S1_mstr;
   input [3-1:0]           S2_mstr;
   input [3-1:0]           S3_mstr;
   input [3-1:0]           S4_mstr;
   input [3-1:0]           S5_mstr;
   
   input                       S0_HREADY;
   input                       S1_HREADY;
   input                       S2_HREADY;
   input                       S3_HREADY;
   input                       S4_HREADY;
   input                       S5_HREADY;
   
   
   output                S0_M0;
   output                S1_M0;
   output                S2_M0;
   output                S3_M0;
   output                S4_M0;
   output                S5_M0;
   output                S0_M1;
   output                S1_M1;
   output                S2_M1;
   output                S3_M1;
   output                S4_M1;
   output                S5_M1;
   output                S0_M2;
   output                S1_M2;
   output                S2_M2;
   output                S3_M2;
   output                S4_M2;
   output                S5_M2;
   output                      S0_M0_resp;
   output                      S1_M0_resp;
   output                      S2_M0_resp;
   output                      S3_M0_resp;
   output                      S4_M0_resp;
   output                      S5_M0_resp;
   output                      S0_M1_resp;
   output                      S1_M1_resp;
   output                      S2_M1_resp;
   output                      S3_M1_resp;
   output                      S4_M1_resp;
   output                      S5_M1_resp;
   output                      S0_M2_resp;
   output                      S1_M2_resp;
   output                      S2_M2_resp;
   output                      S3_M2_resp;
   output                      S4_M2_resp;
   output                      S5_M2_resp;
   


   
   reg [3-1:0]             S0_mstr_resp;
   reg [3-1:0]             S1_mstr_resp;
   reg [3-1:0]             S2_mstr_resp;
   reg [3-1:0]             S3_mstr_resp;
   reg [3-1:0]             S4_mstr_resp;
   reg [3-1:0]             S5_mstr_resp;


   always @(posedge clk or posedge reset)
     if (reset)
       S0_mstr_resp <= #1 {3{1'b0}};
     else if (S0_HREADY)
       S0_mstr_resp <= #1 S0_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S1_mstr_resp <= #1 {3{1'b0}};
     else if (S1_HREADY)
       S1_mstr_resp <= #1 S1_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S2_mstr_resp <= #1 {3{1'b0}};
     else if (S2_HREADY)
       S2_mstr_resp <= #1 S2_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S3_mstr_resp <= #1 {3{1'b0}};
     else if (S3_HREADY)
       S3_mstr_resp <= #1 S3_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S4_mstr_resp <= #1 {3{1'b0}};
     else if (S4_HREADY)
       S4_mstr_resp <= #1 S4_mstr;

   always @(posedge clk or posedge reset)
     if (reset)
       S5_mstr_resp <= #1 {3{1'b0}};
     else if (S5_HREADY)
       S5_mstr_resp <= #1 S5_mstr;


     
   assign               S0_M0      = S0_mstr[0];
   assign               S1_M0      = S1_mstr[0];
   assign               S2_M0      = S2_mstr[0];
   assign               S3_M0      = S3_mstr[0];
   assign               S4_M0      = S4_mstr[0];
   assign               S5_M0      = S5_mstr[0];
   assign               S0_M1      = S0_mstr[1];
   assign               S1_M1      = S1_mstr[1];
   assign               S2_M1      = S2_mstr[1];
   assign               S3_M1      = S3_mstr[1];
   assign               S4_M1      = S4_mstr[1];
   assign               S5_M1      = S5_mstr[1];
   assign               S0_M2      = S0_mstr[2];
   assign               S1_M2      = S1_mstr[2];
   assign               S2_M2      = S2_mstr[2];
   assign               S3_M2      = S3_mstr[2];
   assign               S4_M2      = S4_mstr[2];
   assign               S5_M2      = S5_mstr[2];
   
   assign               S0_M0_resp = S0_mstr_resp[0];
   assign               S1_M0_resp = S1_mstr_resp[0];
   assign               S2_M0_resp = S2_mstr_resp[0];
   assign               S3_M0_resp = S3_mstr_resp[0];
   assign               S4_M0_resp = S4_mstr_resp[0];
   assign               S5_M0_resp = S5_mstr_resp[0];
   assign               S0_M1_resp = S0_mstr_resp[1];
   assign               S1_M1_resp = S1_mstr_resp[1];
   assign               S2_M1_resp = S2_mstr_resp[1];
   assign               S3_M1_resp = S3_mstr_resp[1];
   assign               S4_M1_resp = S4_mstr_resp[1];
   assign               S5_M1_resp = S5_mstr_resp[1];
   assign               S0_M2_resp = S0_mstr_resp[2];
   assign               S1_M2_resp = S1_mstr_resp[2];
   assign               S2_M2_resp = S2_mstr_resp[2];
   assign               S3_M2_resp = S3_mstr_resp[2];
   assign               S4_M2_resp = S4_mstr_resp[2];
   assign               S5_M2_resp = S5_mstr_resp[2];
   
endmodule






