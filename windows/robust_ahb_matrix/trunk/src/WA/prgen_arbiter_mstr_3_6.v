




  
module prgen_arbiter_mstr_3_6(clk,reset,M_last,M_req,M_grant,M0_slave,M1_slave,M2_slave,S0_master,S1_master,S2_master,S3_master,S4_master,S5_master);

   input                   clk;
   input                   reset;

   input [3-1:0]           M_last;
   input [3-1:0]           M_req;
   input [3-1:0]           M_grant;
   
   input [3-1:0]           M0_slave;
   input [3-1:0]           M1_slave;
   input [3-1:0]           M2_slave;
   
   output [3-1:0]           S0_master;
   output [3-1:0]           S1_master;
   output [3-1:0]           S2_master;
   output [3-1:0]           S3_master;
   output [3-1:0]           S4_master;
   output [3-1:0]           S5_master;


   
   reg [3:0]               S0_master_prio_reg;
   reg [3:0]               S1_master_prio_reg;
   reg [3:0]               S2_master_prio_reg;
   reg [3:0]               S3_master_prio_reg;
   reg [3:0]               S4_master_prio_reg;
   reg [3:0]               S5_master_prio_reg;
   wire [3-1:0]               S0_master_prio;
   wire [3-1:0]               S1_master_prio;
   wire [3-1:0]               S2_master_prio;
   wire [3-1:0]               S3_master_prio;
   wire [3-1:0]               S4_master_prio;
   wire [3-1:0]               S5_master_prio;
   reg [3-1:0]               S0_master_d;
   reg [3-1:0]               S1_master_d;
   reg [3-1:0]               S2_master_d;
   reg [3-1:0]               S3_master_d;
   reg [3-1:0]               S4_master_d;
   reg [3-1:0]               S5_master_d;
   
   wire [3-1:0]               M_S0;
   wire [3-1:0]               M_S1;
   wire [3-1:0]               M_S2;
   wire [3-1:0]               M_S3;
   wire [3-1:0]               M_S4;
   wire [3-1:0]               M_S5;
   wire [3-1:0]               M_S0_valid;
   wire [3-1:0]               M_S1_valid;
   wire [3-1:0]               M_S2_valid;
   wire [3-1:0]               M_S3_valid;
   wire [3-1:0]               M_S4_valid;
   wire [3-1:0]               M_S5_valid;
   wire [3-1:0]               M_S0_prio;
   wire [3-1:0]               M_S1_prio;
   wire [3-1:0]               M_S2_prio;
   wire [3-1:0]               M_S3_prio;
   wire [3-1:0]               M_S4_prio;
   wire [3-1:0]               M_S5_prio;
   reg [3-1:0]               M_S0_burst;
   reg [3-1:0]               M_S1_burst;
   reg [3-1:0]               M_S2_burst;
   reg [3-1:0]               M_S3_burst;
   reg [3-1:0]               M_S4_burst;
   reg [3-1:0]               M_S5_burst;


   
   
   parameter                   MASTER_NONE = 3'b000;
   parameter                   MASTER0    = 3'b001;
   parameter                   MASTER1    = 3'b010;
   parameter                   MASTER2    = 3'b100;
   
   
   

   always @(posedge clk or posedge reset)
     if (reset)
       begin
      S0_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S1_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S2_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S3_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S4_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S5_master_prio_reg[3:1] <= #1 {3{1'b0}};
      S0_master_prio_reg[0]          <= #1 1'b1;
      S1_master_prio_reg[0]          <= #1 1'b1;
      S2_master_prio_reg[0]          <= #1 1'b1;
      S3_master_prio_reg[0]          <= #1 1'b1;
      S4_master_prio_reg[0]          <= #1 1'b1;
      S5_master_prio_reg[0]          <= #1 1'b1;
       end
     else if (|(M_req & M_grant & M_last))
       begin      
      S0_master_prio_reg[3:1] <= #1 S0_master_prio_reg[3-1:0];
      S1_master_prio_reg[3:1] <= #1 S1_master_prio_reg[3-1:0];
      S2_master_prio_reg[3:1] <= #1 S2_master_prio_reg[3-1:0];
      S3_master_prio_reg[3:1] <= #1 S3_master_prio_reg[3-1:0];
      S4_master_prio_reg[3:1] <= #1 S4_master_prio_reg[3-1:0];
      S5_master_prio_reg[3:1] <= #1 S5_master_prio_reg[3-1:0];
      S0_master_prio_reg[0]          <= #1 S0_master_prio_reg[3-1];
      S1_master_prio_reg[0]          <= #1 S1_master_prio_reg[3-1];
      S2_master_prio_reg[0]          <= #1 S2_master_prio_reg[3-1];
      S3_master_prio_reg[0]          <= #1 S3_master_prio_reg[3-1];
      S4_master_prio_reg[0]          <= #1 S4_master_prio_reg[3-1];
      S5_master_prio_reg[0]          <= #1 S5_master_prio_reg[3-1];
       end

   assign S0_master_prio = S0_master_prio_reg[3-1:0];
   assign S1_master_prio = S1_master_prio_reg[3-1:0];
   assign S2_master_prio = S2_master_prio_reg[3-1:0];
   assign S3_master_prio = S3_master_prio_reg[3-1:0];
   assign S4_master_prio = S4_master_prio_reg[3-1:0];
   assign S5_master_prio = S5_master_prio_reg[3-1:0];
   
   assign M_S0_prio      = M_S0_valid & S0_master_prio;
   assign M_S1_prio      = M_S1_valid & S1_master_prio;
   assign M_S2_prio      = M_S2_valid & S2_master_prio;
   assign M_S3_prio      = M_S3_valid & S3_master_prio;
   assign M_S4_prio      = M_S4_valid & S4_master_prio;
   assign M_S5_prio      = M_S5_valid & S5_master_prio;
   


   always @(posedge clk or posedge reset)
     if (reset)
       begin
      S0_master_d <= #1 {3{1'b0}};
      S1_master_d <= #1 {3{1'b0}};
      S2_master_d <= #1 {3{1'b0}};
      S3_master_d <= #1 {3{1'b0}};
      S4_master_d <= #1 {3{1'b0}};
      S5_master_d <= #1 {3{1'b0}};
       end
     else
       begin
      S0_master_d <= #1 S0_master;
      S1_master_d <= #1 S1_master;
      S2_master_d <= #1 S2_master;
      S3_master_d <= #1 S3_master;
      S4_master_d <= #1 S4_master;
      S5_master_d <= #1 S5_master;
       end

     always @(posedge clk or posedge reset)                        
       if (reset)                                                  
     begin                                                     
        M_S0_burst[0] <= #1 1'b0;                        
        M_S1_burst[0] <= #1 1'b0;                        
        M_S2_burst[0] <= #1 1'b0;                        
        M_S3_burst[0] <= #1 1'b0;                        
        M_S4_burst[0] <= #1 1'b0;                        
        M_S5_burst[0] <= #1 1'b0;                        
     end                                                       
       else if (M_req[0])                         
     begin                                                     
        M_S0_burst[0] <= #1 S0_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S1_burst[0] <= #1 S1_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S2_burst[0] <= #1 S2_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S3_burst[0] <= #1 S3_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S4_burst[0] <= #1 S4_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
        M_S5_burst[0] <= #1 S5_master[0] & (M_grant[0] ? (~M_last[0]) : 1'b1); 
     end
   
     always @(posedge clk or posedge reset)                        
       if (reset)                                                  
     begin                                                     
        M_S0_burst[1] <= #1 1'b0;                        
        M_S1_burst[1] <= #1 1'b0;                        
        M_S2_burst[1] <= #1 1'b0;                        
        M_S3_burst[1] <= #1 1'b0;                        
        M_S4_burst[1] <= #1 1'b0;                        
        M_S5_burst[1] <= #1 1'b0;                        
     end                                                       
       else if (M_req[1])                         
     begin                                                     
        M_S0_burst[1] <= #1 S0_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S1_burst[1] <= #1 S1_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S2_burst[1] <= #1 S2_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S3_burst[1] <= #1 S3_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S4_burst[1] <= #1 S4_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
        M_S5_burst[1] <= #1 S5_master[1] & (M_grant[1] ? (~M_last[1]) : 1'b1); 
     end
   
     always @(posedge clk or posedge reset)                        
       if (reset)                                                  
     begin                                                     
        M_S0_burst[2] <= #1 1'b0;                        
        M_S1_burst[2] <= #1 1'b0;                        
        M_S2_burst[2] <= #1 1'b0;                        
        M_S3_burst[2] <= #1 1'b0;                        
        M_S4_burst[2] <= #1 1'b0;                        
        M_S5_burst[2] <= #1 1'b0;                        
     end                                                       
       else if (M_req[2])                         
     begin                                                     
        M_S0_burst[2] <= #1 S0_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
        M_S1_burst[2] <= #1 S1_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
        M_S2_burst[2] <= #1 S2_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
        M_S3_burst[2] <= #1 S3_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
        M_S4_burst[2] <= #1 S4_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
        M_S5_burst[2] <= #1 S5_master[2] & (M_grant[2] ? (~M_last[2]) : 1'b1); 
     end
   
      
     assign                              M_S0 = {M2_slave == 'd0 , M1_slave == 'd0 , M0_slave == 'd0};
     assign                              M_S1 = {M2_slave == 'd1 , M1_slave == 'd1 , M0_slave == 'd1};
     assign                              M_S2 = {M2_slave == 'd2 , M1_slave == 'd2 , M0_slave == 'd2};
     assign                              M_S3 = {M2_slave == 'd3 , M1_slave == 'd3 , M0_slave == 'd3};
     assign                              M_S4 = {M2_slave == 'd4 , M1_slave == 'd4 , M0_slave == 'd4};
     assign                              M_S5 = {M2_slave == 'd5 , M1_slave == 'd5 , M0_slave == 'd5};
   
   assign                  M_S0_valid = M_S0 & M_req;
   assign                  M_S1_valid = M_S1 & M_req;
   assign                  M_S2_valid = M_S2 & M_req;
   assign                  M_S3_valid = M_S3 & M_req;
   assign                  M_S4_valid = M_S4 & M_req;
   assign                  M_S5_valid = M_S5 & M_req;
   
   
     assign                    S0_master = 
                               |M_S0_burst ? S0_master_d : 
                                            M_S0_prio[0]  ? MASTER0 :  
                                            M_S0_prio[1]  ? MASTER1 :  
                                            M_S0_prio[2]  ? MASTER2 :  
                            M_S0_valid[0] ? MASTER0 :      
                            M_S0_valid[1] ? MASTER1 :      
                            M_S0_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
     assign                    S1_master = 
                               |M_S1_burst ? S1_master_d : 
                                            M_S1_prio[0]  ? MASTER0 :  
                                            M_S1_prio[1]  ? MASTER1 :  
                                            M_S1_prio[2]  ? MASTER2 :  
                            M_S1_valid[0] ? MASTER0 :      
                            M_S1_valid[1] ? MASTER1 :      
                            M_S1_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
     assign                    S2_master = 
                               |M_S2_burst ? S2_master_d : 
                                            M_S2_prio[0]  ? MASTER0 :  
                                            M_S2_prio[1]  ? MASTER1 :  
                                            M_S2_prio[2]  ? MASTER2 :  
                            M_S2_valid[0] ? MASTER0 :      
                            M_S2_valid[1] ? MASTER1 :      
                            M_S2_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
     assign                    S3_master = 
                               |M_S3_burst ? S3_master_d : 
                                            M_S3_prio[0]  ? MASTER0 :  
                                            M_S3_prio[1]  ? MASTER1 :  
                                            M_S3_prio[2]  ? MASTER2 :  
                            M_S3_valid[0] ? MASTER0 :      
                            M_S3_valid[1] ? MASTER1 :      
                            M_S3_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
     assign                    S4_master = 
                               |M_S4_burst ? S4_master_d : 
                                            M_S4_prio[0]  ? MASTER0 :  
                                            M_S4_prio[1]  ? MASTER1 :  
                                            M_S4_prio[2]  ? MASTER2 :  
                            M_S4_valid[0] ? MASTER0 :      
                            M_S4_valid[1] ? MASTER1 :      
                            M_S4_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
     assign                    S5_master = 
                               |M_S5_burst ? S5_master_d : 
                                            M_S5_prio[0]  ? MASTER0 :  
                                            M_S5_prio[1]  ? MASTER1 :  
                                            M_S5_prio[2]  ? MASTER2 :  
                            M_S5_valid[0] ? MASTER0 :      
                            M_S5_valid[1] ? MASTER1 :      
                            M_S5_valid[2] ? MASTER2 :      
                            MASTER_NONE;
   
      
     endmodule



