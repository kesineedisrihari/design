




module ahb_matrix_3_6_dec (M0_HADDR,M1_HADDR,M2_HADDR,M0_slv,M1_slv,M2_slv);

   input [32-1:0]               M0_HADDR;
   input [32-1:0]               M1_HADDR;
   input [32-1:0]               M2_HADDR;
   output [3-1:0]               M0_slv;
   output [3-1:0]               M1_slv;
   output [3-1:0]               M2_slv;
   
   
   parameter                                  DEC_MSB =  32 - 1;
   parameter                                  DEC_LSB =  32 - 3;
   
   reg [3-1:0]                   M0_slv;
   reg [3-1:0]                   M1_slv;
   reg [3-1:0]                   M2_slv;
   
     always @(*)
       begin                                                  
      case (M0_HADDR[DEC_MSB:DEC_LSB])    
        3'b000 : M0_slv = 'd0;  
        3'b001 : M0_slv = 'd1;  
        3'b010 : M0_slv = 'd2;  
        3'b011 : M0_slv = 'd3;  
        3'b100 : M0_slv = 'd4;  
        3'b101 : M0_slv = 'd5;  

            default : M0_slv = 'd5;                     
      endcase                                             
       end                                                    

     always @(*)
       begin                                                  
      case (M1_HADDR[DEC_MSB:DEC_LSB])    
        3'b000 : M1_slv = 'd0;  
        3'b001 : M1_slv = 'd1;  
        3'b010 : M1_slv = 'd2;  
        3'b011 : M1_slv = 'd3;  
        3'b100 : M1_slv = 'd4;  
        3'b101 : M1_slv = 'd5;  

            default : M1_slv = 'd5;                     
      endcase                                             
       end                                                    

     always @(*)
       begin                                                  
      case (M2_HADDR[DEC_MSB:DEC_LSB])    
        3'b000 : M2_slv = 'd0;  
        3'b001 : M2_slv = 'd1;  
        3'b010 : M2_slv = 'd2;  
        3'b011 : M2_slv = 'd3;  
        3'b100 : M2_slv = 'd4;  
        3'b101 : M2_slv = 'd5;  

            default : M2_slv = 'd5;                     
      endcase                                             
       end                                                    

      
     endmodule





