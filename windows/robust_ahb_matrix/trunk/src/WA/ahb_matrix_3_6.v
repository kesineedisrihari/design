



  
module ahb_matrix_3_6(clk,reset,M0_HADDR,M0_HBURST,M0_HSIZE,M0_HTRANS,M0_HWRITE,M0_HWDATA,M0_HRDATA,M0_HRESP,M0_HREADY,M1_HADDR,M1_HBURST,M1_HSIZE,M1_HTRANS,M1_HWRITE,M1_HWDATA,M1_HRDATA,M1_HRESP,M1_HREADY,M2_HADDR,M2_HBURST,M2_HSIZE,M2_HTRANS,M2_HWRITE,M2_HWDATA,M2_HRDATA,M2_HRESP,M2_HREADY,S0_HADDR,S0_HBURST,S0_HSIZE,S0_HTRANS,S0_HWRITE,S0_HWDATA,S0_HRDATA,S0_HRESP,S0_HREADY,S1_HADDR,S1_HBURST,S1_HSIZE,S1_HTRANS,S1_HWRITE,S1_HWDATA,S1_HRDATA,S1_HRESP,S1_HREADY,S2_HADDR,S2_HBURST,S2_HSIZE,S2_HTRANS,S2_HWRITE,S2_HWDATA,S2_HRDATA,S2_HRESP,S2_HREADY,S3_HADDR,S3_HBURST,S3_HSIZE,S3_HTRANS,S3_HWRITE,S3_HWDATA,S3_HRDATA,S3_HRESP,S3_HREADY,S4_HADDR,S4_HBURST,S4_HSIZE,S4_HTRANS,S4_HWRITE,S4_HWDATA,S4_HRDATA,S4_HRESP,S4_HREADY,S5_HADDR,S5_HBURST,S5_HSIZE,S5_HTRANS,S5_HWRITE,S5_HWDATA,S5_HRDATA,S5_HRESP,S5_HREADY);

   input                clk;
   input                       reset;
   
   input [31:0]                M0_HADDR;
   input [2:0]                 M0_HBURST;
   input [1:0]                 M0_HSIZE;
   input [1:0]                 M0_HTRANS;
   input                       M0_HWRITE;
   input [31:0]                M0_HWDATA;
   output [31:0]               M0_HRDATA;
   output                      M0_HRESP;
   output                      M0_HREADY;
   input [31:0]                M1_HADDR;
   input [2:0]                 M1_HBURST;
   input [1:0]                 M1_HSIZE;
   input [1:0]                 M1_HTRANS;
   input                       M1_HWRITE;
   input [31:0]                M1_HWDATA;
   output [31:0]               M1_HRDATA;
   output                      M1_HRESP;
   output                      M1_HREADY;
   input [31:0]                M2_HADDR;
   input [2:0]                 M2_HBURST;
   input [1:0]                 M2_HSIZE;
   input [1:0]                 M2_HTRANS;
   input                       M2_HWRITE;
   input [31:0]                M2_HWDATA;
   output [31:0]               M2_HRDATA;
   output                      M2_HRESP;
   output                      M2_HREADY;
   output [31:0]               S0_HADDR;
   output [2:0]                S0_HBURST;
   output [1:0]                S0_HSIZE;
   output [1:0]                S0_HTRANS;
   output                      S0_HWRITE;
   output [31:0]               S0_HWDATA;
   input [31:0]                S0_HRDATA;
   input                       S0_HRESP;
   input                       S0_HREADY;
   output [31:0]               S1_HADDR;
   output [2:0]                S1_HBURST;
   output [1:0]                S1_HSIZE;
   output [1:0]                S1_HTRANS;
   output                      S1_HWRITE;
   output [31:0]               S1_HWDATA;
   input [31:0]                S1_HRDATA;
   input                       S1_HRESP;
   input                       S1_HREADY;
   output [31:0]               S2_HADDR;
   output [2:0]                S2_HBURST;
   output [1:0]                S2_HSIZE;
   output [1:0]                S2_HTRANS;
   output                      S2_HWRITE;
   output [31:0]               S2_HWDATA;
   input [31:0]                S2_HRDATA;
   input                       S2_HRESP;
   input                       S2_HREADY;
   output [31:0]               S3_HADDR;
   output [2:0]                S3_HBURST;
   output [1:0]                S3_HSIZE;
   output [1:0]                S3_HTRANS;
   output                      S3_HWRITE;
   output [31:0]               S3_HWDATA;
   input [31:0]                S3_HRDATA;
   input                       S3_HRESP;
   input                       S3_HREADY;
   output [31:0]               S4_HADDR;
   output [2:0]                S4_HBURST;
   output [1:0]                S4_HSIZE;
   output [1:0]                S4_HTRANS;
   output                      S4_HWRITE;
   output [31:0]               S4_HWDATA;
   input [31:0]                S4_HRDATA;
   input                       S4_HRESP;
   input                       S4_HREADY;
   output [31:0]               S5_HADDR;
   output [2:0]                S5_HBURST;
   output [1:0]                S5_HSIZE;
   output [1:0]                S5_HTRANS;
   output                      S5_HWRITE;
   output [31:0]               S5_HWDATA;
   input [31:0]                S5_HRDATA;
   input                       S5_HRESP;
   input                       S5_HREADY;

   

   wire [3-1:0]            S0_mstr;
   wire [3-1:0]            S1_mstr;
   wire [3-1:0]            S2_mstr;
   wire [3-1:0]            S3_mstr;
   wire [3-1:0]            S4_mstr;
   wire [3-1:0]            S5_mstr;
   
   wire [3-1:0]         M0_slv;
   wire [3-1:0]         M1_slv;
   wire [3-1:0]         M2_slv;

   wire                        M0_HLAST;
   wire                        M1_HLAST;
   wire                        M2_HLAST;
   
   wire                        S0_M0;
   wire                        S1_M0;
   wire                        S2_M0;
   wire                        S3_M0;
   wire                        S4_M0;
   wire                        S5_M0;
   wire                        S0_M1;
   wire                        S1_M1;
   wire                        S2_M1;
   wire                        S3_M1;
   wire                        S4_M1;
   wire                        S5_M1;
   wire                        S0_M2;
   wire                        S1_M2;
   wire                        S2_M2;
   wire                        S3_M2;
   wire                        S4_M2;
   wire                        S5_M2;
   wire                        S0_M0_resp;
   wire                        S1_M0_resp;
   wire                        S2_M0_resp;
   wire                        S3_M0_resp;
   wire                        S4_M0_resp;
   wire                        S5_M0_resp;
   wire                        S0_M1_resp;
   wire                        S1_M1_resp;
   wire                        S2_M1_resp;
   wire                        S3_M1_resp;
   wire                        S4_M1_resp;
   wire                        S5_M1_resp;
   wire                        S0_M2_resp;
   wire                        S1_M2_resp;
   wire                        S2_M2_resp;
   wire                        S3_M2_resp;
   wire                        S4_M2_resp;
   wire                        S5_M2_resp;


   
   ahb_matrix_3_6_dec
   ahb_matrix_3_6_dec (
                  .M0_HADDR(M0_HADDR),
                  .M1_HADDR(M1_HADDR),
                  .M2_HADDR(M2_HADDR),
                  .M0_slv(M0_slv),
                  .M1_slv(M1_slv),
                  .M2_slv(M2_slv)
                  );


     ahb_matrix_3_6_hlast
       ahb_matrix_3_6_hlast(
                        .clk(clk),
                        .reset(reset),
                        .M0_HTRANS(M0_HTRANS),
                        .M1_HTRANS(M1_HTRANS),
                        .M2_HTRANS(M2_HTRANS),
                        .M0_HREADY(M0_HREADY),
                        .M1_HREADY(M1_HREADY),
                        .M2_HREADY(M2_HREADY),
                        .M0_HBURST(M0_HBURST),
                        .M1_HBURST(M1_HBURST),
                        .M2_HBURST(M2_HBURST),
                        .M0_HLAST(M0_HLAST),
                        .M1_HLAST(M1_HLAST),
                        .M2_HLAST(M2_HLAST)
                        );
   
   
   prgen_arbiter_mstr_3_6
   prgen_arbiter_mstr_3_6(
                                 .clk(clk),
                                 .reset(reset),
      
                                 .M0_slave(M0_slv),
                                 .M1_slave(M1_slv),
                                 .M2_slave(M2_slv),
                                 
                                 .S0_master(S0_mstr),
                                 .S1_master(S1_mstr),
                                 .S2_master(S2_mstr),
                                 .S3_master(S3_mstr),
                                 .S4_master(S4_mstr),
                                 .S5_master(S5_mstr),
                                 
                                 .M_last({M2_HLAST , M1_HLAST , M0_HLAST}),
                                 .M_req({M2_HTRANS[1] , M1_HTRANS[1] , M0_HTRANS[1]}),
                                 .M_grant({M2_HREADY , M1_HREADY , M0_HREADY})
                                 );
   

   ahb_matrix_3_6_sel  
     ahb_matrix_3_6_sel (
             .clk(clk),
             .reset(reset),
             .S0_mstr(S0_mstr),
             .S1_mstr(S1_mstr),
             .S2_mstr(S2_mstr),
             .S3_mstr(S3_mstr),
             .S4_mstr(S4_mstr),
             .S5_mstr(S5_mstr),
             .S0_HREADY(S0_HREADY),
             .S1_HREADY(S1_HREADY),
             .S2_HREADY(S2_HREADY),
             .S3_HREADY(S3_HREADY),
             .S4_HREADY(S4_HREADY),
             .S5_HREADY(S5_HREADY),
             .S0_M0(S0_M0),
             .S1_M0(S1_M0),
             .S2_M0(S2_M0),
             .S3_M0(S3_M0),
             .S4_M0(S4_M0),
             .S5_M0(S5_M0),
             .S0_M1(S0_M1),
             .S1_M1(S1_M1),
             .S2_M1(S2_M1),
             .S3_M1(S3_M1),
             .S4_M1(S4_M1),
             .S5_M1(S5_M1),
             .S0_M2(S0_M2),
             .S1_M2(S1_M2),
             .S2_M2(S2_M2),
             .S3_M2(S3_M2),
             .S4_M2(S4_M2),
             .S5_M2(S5_M2),
             .S0_M0_resp(S0_M0_resp),
             .S1_M0_resp(S1_M0_resp),
             .S2_M0_resp(S2_M0_resp),
             .S3_M0_resp(S3_M0_resp),
             .S4_M0_resp(S4_M0_resp),
             .S5_M0_resp(S5_M0_resp),
             .S0_M1_resp(S0_M1_resp),
             .S1_M1_resp(S1_M1_resp),
             .S2_M1_resp(S2_M1_resp),
             .S3_M1_resp(S3_M1_resp),
             .S4_M1_resp(S4_M1_resp),
             .S5_M1_resp(S5_M1_resp),
             .S0_M2_resp(S0_M2_resp),
             .S1_M2_resp(S1_M2_resp),
             .S2_M2_resp(S2_M2_resp),
             .S3_M2_resp(S3_M2_resp),
             .S4_M2_resp(S4_M2_resp),
             .S5_M2_resp(S5_M2_resp)
             );

   
   ahb_matrix_3_6_bus  
     ahb_matrix_3_6_bus (
             .clk(clk),
             .reset(reset),
                     .M0_HADDR(M0_HADDR),
                     .M0_HBURST(M0_HBURST),
                     .M0_HSIZE(M0_HSIZE),
                     .M0_HTRANS(M0_HTRANS),
                     .M0_HWRITE(M0_HWRITE),
                     .M0_HWDATA(M0_HWDATA),
                     .M0_HRDATA(M0_HRDATA),
                     .M0_HRESP(M0_HRESP),
                     .M0_HREADY(M0_HREADY),
                     .M1_HADDR(M1_HADDR),
                     .M1_HBURST(M1_HBURST),
                     .M1_HSIZE(M1_HSIZE),
                     .M1_HTRANS(M1_HTRANS),
                     .M1_HWRITE(M1_HWRITE),
                     .M1_HWDATA(M1_HWDATA),
                     .M1_HRDATA(M1_HRDATA),
                     .M1_HRESP(M1_HRESP),
                     .M1_HREADY(M1_HREADY),
                     .M2_HADDR(M2_HADDR),
                     .M2_HBURST(M2_HBURST),
                     .M2_HSIZE(M2_HSIZE),
                     .M2_HTRANS(M2_HTRANS),
                     .M2_HWRITE(M2_HWRITE),
                     .M2_HWDATA(M2_HWDATA),
                     .M2_HRDATA(M2_HRDATA),
                     .M2_HRESP(M2_HRESP),
                     .M2_HREADY(M2_HREADY),
                     .S0_HADDR(S0_HADDR),
                     .S0_HBURST(S0_HBURST),
                     .S0_HSIZE(S0_HSIZE),
                     .S0_HTRANS(S0_HTRANS),
                     .S0_HWRITE(S0_HWRITE),
                     .S0_HWDATA(S0_HWDATA),
                     .S0_HRDATA(S0_HRDATA),
                     .S0_HRESP(S0_HRESP),
                     .S0_HREADY(S0_HREADY),
                     .S1_HADDR(S1_HADDR),
                     .S1_HBURST(S1_HBURST),
                     .S1_HSIZE(S1_HSIZE),
                     .S1_HTRANS(S1_HTRANS),
                     .S1_HWRITE(S1_HWRITE),
                     .S1_HWDATA(S1_HWDATA),
                     .S1_HRDATA(S1_HRDATA),
                     .S1_HRESP(S1_HRESP),
                     .S1_HREADY(S1_HREADY),
                     .S2_HADDR(S2_HADDR),
                     .S2_HBURST(S2_HBURST),
                     .S2_HSIZE(S2_HSIZE),
                     .S2_HTRANS(S2_HTRANS),
                     .S2_HWRITE(S2_HWRITE),
                     .S2_HWDATA(S2_HWDATA),
                     .S2_HRDATA(S2_HRDATA),
                     .S2_HRESP(S2_HRESP),
                     .S2_HREADY(S2_HREADY),
                     .S3_HADDR(S3_HADDR),
                     .S3_HBURST(S3_HBURST),
                     .S3_HSIZE(S3_HSIZE),
                     .S3_HTRANS(S3_HTRANS),
                     .S3_HWRITE(S3_HWRITE),
                     .S3_HWDATA(S3_HWDATA),
                     .S3_HRDATA(S3_HRDATA),
                     .S3_HRESP(S3_HRESP),
                     .S3_HREADY(S3_HREADY),
                     .S4_HADDR(S4_HADDR),
                     .S4_HBURST(S4_HBURST),
                     .S4_HSIZE(S4_HSIZE),
                     .S4_HTRANS(S4_HTRANS),
                     .S4_HWRITE(S4_HWRITE),
                     .S4_HWDATA(S4_HWDATA),
                     .S4_HRDATA(S4_HRDATA),
                     .S4_HRESP(S4_HRESP),
                     .S4_HREADY(S4_HREADY),
                     .S5_HADDR(S5_HADDR),
                     .S5_HBURST(S5_HBURST),
                     .S5_HSIZE(S5_HSIZE),
                     .S5_HTRANS(S5_HTRANS),
                     .S5_HWRITE(S5_HWRITE),
                     .S5_HWDATA(S5_HWDATA),
                     .S5_HRDATA(S5_HRDATA),
                     .S5_HRESP(S5_HRESP),
                     .S5_HREADY(S5_HREADY),
             .S0_M0(S0_M0),
             .S1_M0(S1_M0),
             .S2_M0(S2_M0),
             .S3_M0(S3_M0),
             .S4_M0(S4_M0),
             .S5_M0(S5_M0),
             .S0_M1(S0_M1),
             .S1_M1(S1_M1),
             .S2_M1(S2_M1),
             .S3_M1(S3_M1),
             .S4_M1(S4_M1),
             .S5_M1(S5_M1),
             .S0_M2(S0_M2),
             .S1_M2(S1_M2),
             .S2_M2(S2_M2),
             .S3_M2(S3_M2),
             .S4_M2(S4_M2),
             .S5_M2(S5_M2),
             .S0_M0_resp(S0_M0_resp),
             .S1_M0_resp(S1_M0_resp),
             .S2_M0_resp(S2_M0_resp),
             .S3_M0_resp(S3_M0_resp),
             .S4_M0_resp(S4_M0_resp),
             .S5_M0_resp(S5_M0_resp),
             .S0_M1_resp(S0_M1_resp),
             .S1_M1_resp(S1_M1_resp),
             .S2_M1_resp(S2_M1_resp),
             .S3_M1_resp(S3_M1_resp),
             .S4_M1_resp(S4_M1_resp),
             .S5_M1_resp(S5_M1_resp),
             .S0_M2_resp(S0_M2_resp),
             .S1_M2_resp(S1_M2_resp),
             .S2_M2_resp(S2_M2_resp),
             .S3_M2_resp(S3_M2_resp),
             .S4_M2_resp(S4_M2_resp),
             .S5_M2_resp(S5_M2_resp)
             );
   

   
  
endmodule






