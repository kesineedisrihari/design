



module  axi2ahb_rd_fifo (clk,reset,RID,RDATA,RRESP,RLAST,RVALID,RREADY,HRDATA,HREADY,HTRANS,HRESP,cmd_id,cmd_err,rdata_phase,rdata_ready,data_last);

   parameter              FIFO_LINES = 32; //double buffer of max burst
   parameter              RESP_SLVERR = 2'b10;

   input                  clk;
   input                  reset;

   output [3:0]           RID;
   output [31:0]          RDATA;
   output [1:0]           RRESP;
   output                 RLAST;
   output                 RVALID;
   input                  RREADY;
   input [32-1:0]  HRDATA;
   input                  HREADY;
   input [1:0]            HTRANS;
   input                  HRESP;
   
   input [4-1:0]    cmd_id;
   input                  cmd_err;
   input                  rdata_phase;
   output                 rdata_ready;
   input                  data_last;


   wire                   data_push;
   wire                   data_pop;
   wire                   data_empty;
   wire                   data_full;

   reg                    RVALID;
   
   reg [2:0] burst_cnt;

   wire                   axi_last;
   wire                   ahb_last;
   wire [1:0]             cmd_resp;

   
   assign                 cmd_resp = cmd_err | HRESP ? RESP_SLVERR : 2'b00;
   
   assign                 rdata_ready = burst_cnt < 'd2;
   
   assign                 data_push = rdata_phase & HREADY;
   assign                 data_pop = RVALID & RREADY;
   
   assign                 axi_last = RVALID & RREADY & RLAST;
   assign                 ahb_last = rdata_phase & data_last;
   
   always @(posedge clk or posedge reset)
     if (reset)
       burst_cnt <= #1 'd0;
     else if (axi_last | ahb_last)
       burst_cnt <= #1 burst_cnt - axi_last + ahb_last;
   
   prgen_fifo #(32+4+2+1, FIFO_LINES) 
   data_fifo(
        .clk(clk),
        .reset(reset),
        .push(data_push),
        .pop(data_pop),
        .din({HRDATA,
                  cmd_id,
                  cmd_resp,
                  ahb_last
          }
         ),
        .dout({RDATA,
                   RID,
                   RRESP,
                   RLAST
           }
          ),
        .empty(data_empty),
        .full(data_full)
        );


   always @(posedge clk or posedge reset)
     if (reset)
       RVALID <= #1 1'b0;
     else if (axi_last)
       RVALID <= #1 1'b0;
     else if (burst_cnt > 'd0)
       RVALID <= #1 1'b1;
     else
       RVALID <= #1 1'b0;
   

endmodule

   


